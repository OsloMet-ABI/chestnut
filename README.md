# chestnut
chestnut is a simple JavaFX application that can be used to explore some of the
principles behind PKI. The following functionality is supported:
 
 * Create/load private/public keys
 * Sign data with private key and verify with public key
 * Encrypt data with a public key and decrypt with private key
 * Signing/Encryption/Decryption using XMLDigSignature
 * Symmetric encryption/decryption using AES and ROT-13
 * Checksum generator for data input and files
 * Base64 examples
 * UUID (just a bonus for our students)

If you are a OsloMet student, the software should be available to install 
from the software centre on windows machines. 

## Status
The basic application is complete. We are locked to RSA at the moment. The 
JavaFX code (ChestnutApplication) needs to be restructured and have comments
applied. 

# Building and running
This is a java application. To build from source, you need java and maven 
installed. The project is developed on a Linux machine with Apache Maven 3.2
.1 and Java 1.8.  

    java -version
    mvn --version
  
Getting the code

The latest version of the code is available on Gitlab at
[OsloMet-ABI/chestnut](https://gitlab.com/OsloMet-ABI/chestnut). 


You can clone the project from :  
     
    git clone https://gitlab.com/OsloMet-ABI/chestnut.git

You can build the jar using the following maven command:

    mvn clean package 

You should see information about tests being run and that th application has 
been complied to *target/chestnut-pki-0.0.1-jar-with-dependencies.jar*. This is
a self contained jar file.

You can run the application using

    java -jar target/chestnut-pki-0.0.1-jar-with-dependencies.jar

## Pre-compiled jar
While you should never trust software downloaded from the Internet, we have 
made a jar file available here if you want to quickly download and test 
chestnut. You can download [package/chestnut-pki-0.0.1-jar-with-dependencies.jar](package/chestnut-pki-0.0.1-jar-with-dependencies.jar) 
and run using:

    java -jar chestnut-pki-0.0.1-jar-with-dependencies.jar 

The sha-256 checksum of the above jar file at creation/uploading time was:

    0c7c2eb4de4d7f5e9735e7812da1d10f3b44c54a9c8531de306cb53c277c8a73

## Building native executables

### Linux
There is a script in the project root directory called *package.sh*. If you run
this script it will build a Linux binary. Note this takes a few minutes as it
also creates a .deb package. The executable file can be found in:

    package/chestnut/chestnut 

### Windows
There is also script in the project root directory called *package.bat*. If you
run this script it will build a windows executable installer. Note the 
installer can be found in:
 
    package\bundles

Note: Undertaking this on windows may trigger a requirement for the existence 
of a program called iscc.exe. This can be downloaded from:

    http://jrsoftware.org/isdl.php

## Tests
The back-end functionality described above is tested and the tests show if the 
codebase passes or not. From the root of the project you can run 

    mvn test

You should see some build information followed by log information showing 
what's happening. The test should end up with something like:

    Tests run: 6, Failures: 0, Errors: 0, Skipped: 0

    [INFO] ---------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ---------------------------------------------------------------------


## Why chestnut?
I teach PKI as part of a course that covers digitisation and authenticity of 
records at OsloMet. Typically this has been covered using Linux command-line 
tools but we spend too much time writing commands. This distracts from the 
teaching goals as the students get hung up on the details of the command-line.

## Contributing
I have limited time to spend on this project so you are more than welcome to 
contribute. You can do some of the stuff listed in future work if you want to
help, or you can check/tidy the code. If you could create a version of 
package that works on Mac, that would be great!   

## Future work
The front-end and back-end are separated appropriately so we could consider 
creating this as an online tool. We could perhaps swap this out with  web-page
version using something like [PKIJS](https://pkijs.org/). 
 
 * Create a key-store. Simply use a java key store, but need a save/look for 
 it in a standardised way
 * Code tidying
 * Better support for other algorithms both symmetrical and asymmetrical
 * Perhaps swap out the XML signing with a hybrid encryption process
 * Keep XML signing but add validation check using xsd

## Other
Thanks to IntelliJ for an [idea](https://www.jetbrains.com/idea/) license.
