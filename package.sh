#!/usr/bin/env bash

# Rebuild the project first
mvn clean package

# Try to package everything as a single executable
# The executable will be in package/chestnut/chestnut
javapackager -deploy -title "Chestnut-PKI" -name "chestnut" -appclass chestnut.ChestnutApplication -native -outdir package -outfile test -srcdir "target"

