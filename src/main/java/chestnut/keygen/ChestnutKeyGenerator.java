package chestnut.keygen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class ChestnutKeyGenerator {

    private static final Logger logger =
            LoggerFactory.getLogger(ChestnutKeyGenerator.class);

    private KeyPair keyPair;

    public ChestnutKeyGenerator() {
    }

    public ChestnutKeyGenerator(String privateKey, String publicKey,
                                String algorithm)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] publicKeyBytes = Base64.getDecoder().decode(publicKey);
        byte[] privateKeyBytes = Base64.getDecoder().decode(privateKey);
        X509EncodedKeySpec keySpecPublic =
                new X509EncodedKeySpec(publicKeyBytes);
        PKCS8EncodedKeySpec keySpecPrivate =
                new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        keyPair = new KeyPair(keyFactory.generatePublic(keySpecPublic),
                keyFactory.generatePrivate(keySpecPrivate));
    }

    public ChestnutKeyGenerator(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public void generateKeyPair(String algorithm, int keySize) throws
            NoSuchAlgorithmException {

        KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
        kpg.initialize(keySize);
        keyPair = kpg.generateKeyPair();
        logger.info("Created a key pair set of type [" +
                keyPair.getPrivate().getAlgorithm() + "], size [" +
                keySize + "]");
    }

}
