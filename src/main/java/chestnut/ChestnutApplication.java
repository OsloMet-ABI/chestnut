package chestnut;

import chestnut.controller.DecryptionController;
import chestnut.controller.EncryptionController;
import chestnut.controller.KeyController;
import chestnut.controller.SignatureController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.xml.sax.SAXException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.*;

import static chestnut.util.Constants.*;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Created by tsodring on 31/08/2018
 */
public class ChestnutApplication extends Application {

    private Stage stage;

    private KeyController keyController;
    private SignatureController signatureController;
    private EncryptionController encryptionController;
    private DecryptionController decryptionController;


    private ListView<Label> ownKeyList = new ListView<>();
    private ListView<Label> publicKeyList = new ListView<>();

    private TextArea textDataForSigning;
    private TextArea textDataSignature;
    private TextArea textDataToEncrypt;
    private TextArea textDataEncrypted;
    private TextArea textDataToDecrypt;
    private TextArea textDataDecrypted;
    private TextArea textToDocumentForSigning;
    private TextArea textToDocumentSignature;
    private TextArea textFromDocumentForSigning;
    private TextArea textFromDocumentSignature;

    private TextArea textPrivateKey;
    private TextArea textPublicKey;
    private TextField textKeyType;

    // Checksum fields
    private TextField textMd5;
    private TextField textSha1;
    private TextArea textSha256;
    private TextArea textSha512;
    private TextField textFileLocationForChecksum;
    private TextArea textDataForChecksum;

    // Base64 fields
    private TextArea textBase64From;
    private TextArea textBase64Encoded;
    private TextArea textBase64EncodedFrom;
    private TextArea textBase64Decoded;

    // Symmetric encryption fields
    private TextArea textROT13From;
    private TextArea textROT13To;
    private TextArea textROT13Decrypted;
    private TextArea textROT13Encrypted;

    private TextField textAESKeyEncrypt;
    private TextField textAESKeyDecrypt;
    private TextArea textAESPlainText;
    private TextArea textAESFromEncrypted;
    private TextArea textAESEncrypted;
    private TextArea textAESFDecrypted;

    private TextArea textDataForXMLEnc;
    private TextArea textDataXMLSymmetricEnc;
    private TextArea textDataXMLAsymmetricEnc;

    private TextArea textDataForXMLDec;
    private TextArea textDataXMLAsymmetricDec;
    private TextArea textDataXMLSymmetricDec;

    // UUID fields
    private TextArea textUUID;
    private ComboBox<String> howManyComboBox;

    private StringBuilder logInfo = new StringBuilder();
    private Button buttonVerifiedSignature = new Button();
    private Button buttonVerifiedDocumentSignature = new Button();
    private Button buttonVerifiedFromDocumentSignature = new Button();
    private Button buttonEncryptionStatus = new Button();
    private Button buttonDecryptionStatus = new Button();

    private Button buttonReSign = new Button("Re-sign data");
    private Button buttonReEncrypt = new Button("Re-encrypt data");
    private Button buttonReDecrypt = new Button("Re-decrypt data");

    private TabPane operationsPane;
    private Label statusBar;

    private Tab signingTab;
    private Tab encryptionTab;
    private Tab decryptionTab;
    private Tab xmlSigningTab;
    private Tab xmlEncryptionTab;
    private Tab xmlDecryptionTab;

    private Tooltip tooltipGenerateKeys = new Tooltip(TT_GENERATE_KEY);
    private Tooltip tooltipLoadKeys = new Tooltip(TT_LOAD_KEY);
    private Tooltip tooltipLoadPublicKey = new Tooltip(TT_LOAD_PUBLIC);
    private Tooltip tooltipKeysPanel = new Tooltip(TT_KEY_PANEL);
    private Tooltip tooltipKeysTab = new Tooltip(TT_KEYS_TAB);
    private Tooltip tooltipSigningTab = new Tooltip(TT_SIGNING_TAB);
    private Tooltip tooltipEncryptionTab = new Tooltip(TT_ENCRYPTION_TAB);
    private Tooltip tooltipDecryptionTab = new Tooltip(TT_DECRYPTION_TAB);
    private Tooltip tooltipXMLSigningTab = new Tooltip(TT_XML_SIGNING_TAB);
    private Tooltip tooltipXMLEncryptionTab = new Tooltip(TT_XML_ENC_TAB);
    private Tooltip tooltipXMLDecryptionTab = new Tooltip(TT_XML_DEC_TAB);
    private Tooltip tooltipSymmetricEnc = new Tooltip(TT_SYMMETRIC_ENC_TAB);
    private Tooltip tooltipSymmetricDec = new Tooltip(TT_SYMMETRIC_DEC_TAB);
    private Tooltip tooltipChecksumTab = new Tooltip(TT_CHECKSUM_TAB);
    private Tooltip tooltipBase64Tab = new Tooltip(TT_BASE64_TAB);
    private Tooltip tooltipUUIDTab = new Tooltip(TT_UUID_TAB);

    public static void main(String[] args) {
        launch(ChestnutApplication.class, args);
    }

    @Override
    public void start(Stage stage) {

        this.stage = stage;
        stage.setTitle("chestnut@oslomet");
        BorderPane layout = new BorderPane();
        layout.setPadding(new Insets(20, 0, 20, 20));
        // About Menu
        MenuBar menuBar = new MenuBar();
        menuBar.setStyle("-fx-background-color: transparent;");
        Menu menuAbout = new Menu("About");
        menuAbout.setStyle("-fx-background-color: transparent;");
        MenuItem about = new MenuItem("About chestnut");
        about.setOnAction(this::handleViewAbout);
        menuAbout.getItems().addAll(about);

        // Log Menu
        Menu menuViewLog = new Menu("Log");
        menuViewLog.setStyle("-fx-background-color: transparent;");
        MenuItem log = new MenuItem("View log");
        menuViewLog.getItems().addAll(log);
        log.setOnAction(this::handleViewLog);


        Region filler = new Region();
        filler.setStyle("-fx-background-color: transparent;");
        HBox.setHgrow(filler, Priority.SOMETIMES);
        HBox menubars = new HBox(filler, menuBar);

        menuBar.getMenus().addAll(menuAbout, menuViewLog);
        layout.setTop(menubars);

        // Left pane: Key management
        AnchorPane keysManagerPanel = new AnchorPane();

        // Users own keys
        VBox ownKeysPanel = new VBox();
        ownKeyList.setOnMouseClicked(this::handleOwnKeysPanelSelect);
        ownKeyList.setOnKeyPressed(this::handleOwnKeysPanelSelect);
        ownKeyList.setTooltip(tooltipKeysPanel);

        HBox keyButtons = new HBox();
        Button createKeyPairButton = new Button();
        createKeyPairButton.setText("Generate");
        createKeyPairButton.setStyle(BUTTON);
        createKeyPairButton.setTooltip(tooltipGenerateKeys);
        createKeyPairButton.setOnAction(this::handleGenerateKeyPair);

        Button loadKeyPairButton = new Button();
        loadKeyPairButton.setText("Load keypair");
        loadKeyPairButton.setStyle(BUTTON);
        loadKeyPairButton.setOnAction(this::handleLoadKeyPair);
        loadKeyPairButton.setTooltip(tooltipLoadKeys);
        keyButtons.getChildren().addAll(createKeyPairButton, loadKeyPairButton);

        Label labelKeypairs = new Label("Keypairs");
        labelKeypairs.setStyle("-fx-font-size: 14; -fx-text-fill: #337ab7");
        ownKeysPanel.prefHeightProperty().bind(
                stage.heightProperty()
                        .multiply(0.40));
        ownKeysPanel.getChildren().addAll(labelKeypairs, ownKeyList, keyButtons);

        // Users imported keys
        VBox publicKeysPanel = new VBox();

        publicKeyList.setOnMouseClicked(this::handlePublicKeysPanelSelect);
        publicKeyList.setOnKeyPressed(this::handlePublicKeysPanelSelect);
        publicKeyList.setTooltip(tooltipKeysPanel);

        Button addPublicKeyButton = new Button();
        addPublicKeyButton.setText("Add public key");
        addPublicKeyButton.setOnAction(this::handleLoadPublicKey);
        addPublicKeyButton.setStyle(BUTTON);
        addPublicKeyButton.setTooltip(tooltipLoadPublicKey);

        Label labelPublicKeys = new Label("Imported public keys");
        labelPublicKeys.setStyle("-fx-font-size: 14; -fx-text-fill: #337ab7");

        publicKeysPanel.prefHeightProperty().bind(
                stage.heightProperty()
                        .multiply(0.40));
        publicKeysPanel.getChildren().addAll(labelPublicKeys,
                publicKeyList, addPublicKeyButton);


        AnchorPane.setTopAnchor(ownKeysPanel, 2.0);
        AnchorPane.setBottomAnchor(publicKeysPanel, 2.0);

        keysManagerPanel.getChildren().addAll(ownKeysPanel,
                publicKeysPanel);

        keysManagerPanel.setPrefSize(300, 720);
        layout.setLeft(keysManagerPanel);

        Pane operationsPanel = new Pane();

        operationsPane = new TabPane();
        operationsPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        operationsPane.setPrefSize(1000, 720);

        // Keys tab
        Tab keysTab = new Tab();
        keysTab.setStyle(BACKGROUND);
        keysTab.setText("Keys");
        keysTab.setTooltip(tooltipKeysTab);

        GridPane keysGridPane = new GridPane();
        keysGridPane.setStyle(BACKGROUND);
        keysGridPane.setPadding(new Insets(5));
        keysGridPane.setHgap(5);
        keysGridPane.setVgap(5);
        ColumnConstraints keysColumn1 = new ColumnConstraints();
        keysColumn1.setPercentWidth(45);
        ColumnConstraints keysColumn2 = new ColumnConstraints();
        keysColumn2.setPercentWidth(45);
        keysColumn2.setHgrow(Priority.ALWAYS);
        keysGridPane.getColumnConstraints().addAll(keysColumn1, keysColumn2);
        keysGridPane.setStyle(BACKGROUND);

        setRowConstraints(keysGridPane);

        // Show the key type
        Label labelKeyType = new Label("Key type");
        keysGridPane.add(labelKeyType, 0, 1);
        textKeyType = new TextField();
        textKeyType.setEditable(false);
        keysGridPane.add(textKeyType, 0, 2);

        // Show the contents of the private key
        Label labelPrivateKey = new Label("Private key");
        keysGridPane.add(labelPrivateKey, 0, 3);
        textPrivateKey = new TextArea();
        textPrivateKey.setWrapText(true);
        textPrivateKey.setEditable(false);
        keysGridPane.add(textPrivateKey, 0, 4, 1, 16);

        // Show the contents of the public key
        Label labelPublicKey = new Label("Public key");
        keysGridPane.add(labelPublicKey, 1, 3);
        textPublicKey = new TextArea();
        textPublicKey.setEditable(false);
        textPublicKey.setWrapText(true);
        keysGridPane.add(textPublicKey, 1, 4, 1, 6);

        // Create buttons
        Button savePrivateKeyButton = new Button("Save private key");
        savePrivateKeyButton.setOnAction(this::handleSavePrivateKey);
        savePrivateKeyButton.setStyle(BUTTON);
        GridPane.setHalignment(savePrivateKeyButton, HPos.RIGHT);
        keysGridPane.add(savePrivateKeyButton, 0, 21);

        Button savePublicKeyButton = new Button("Save public key");
        savePublicKeyButton.setOnAction(this::handleSavePublicKey);
        savePublicKeyButton.setStyle(BUTTON);
        GridPane.setHalignment(savePublicKeyButton, HPos.RIGHT);
        keysGridPane.add(savePublicKeyButton, 1, 11);

        keysTab.setContent(keysGridPane);

        // Create Signing tab
        signingTab = new Tab();
        signingTab.setText(SIGNATURE);
        signingTab.setStyle(BACKGROUND);
        signingTab.setTooltip(tooltipSigningTab);
        // disabled until keys are created
        signingTab.setDisable(true);

        GridPane signingGridPane = new GridPane();
        signingGridPane.setStyle(BACKGROUND);
        signingGridPane.setPadding(new Insets(5));
        signingGridPane.setHgap(5);
        signingGridPane.setVgap(5);
        ColumnConstraints signColumn1 = new ColumnConstraints();
        signColumn1.setPercentWidth(45);
        ColumnConstraints signColumn2 = new ColumnConstraints();
        signColumn2.setPercentWidth(45);
        signingGridPane.getColumnConstraints().addAll(signColumn1, signColumn2);
        setRowConstraints(signingGridPane);
        signingGridPane.add(new Label(""), 0, 0);

        // Data box to enter data into
        Label labelDataForSigning = new Label("Data to sign");
        signingGridPane.add(labelDataForSigning, 0, 1);
        textDataForSigning = new TextArea();
        textDataForSigning.setWrapText(true);
        signingGridPane.add(textDataForSigning, 0, 2, 1, 8);

        // Signature box to show signature
        Label labelDataSignature = new Label("Signature");
        signingGridPane.add(labelDataSignature, 1, 1);
        textDataSignature = new TextArea();
        textDataSignature.setEditable(false);
        textDataSignature.setWrapText(true);
        signingGridPane.add(textDataSignature, 1, 2, 1, 8);

        Button verifyDataButton = new Button("Verify signature");
        verifyDataButton.setStyle(BUTTON);
        verifyDataButton.setOnAction(this::handleVerifySignedData);
        GridPane.setHalignment(verifyDataButton, HPos.RIGHT);
        signingGridPane.add(verifyDataButton, 1, 11);

        buttonReSign.setStyle(BUTTON);
        GridPane.setHalignment(buttonReSign, HPos.RIGHT);
        buttonReSign.setVisible(false);
        signingGridPane.add(buttonReSign, 0, 11);

        buttonVerifiedSignature.setMaxWidth(Double.MAX_VALUE);
        buttonVerifiedSignature.setText("Signature is correct!");
        buttonVerifiedSignature.setVisible(false);
        GridPane.setColumnSpan(buttonVerifiedSignature, 2);
        signingGridPane.add(buttonVerifiedSignature, 0, 13);
        signingTab.setContent(signingGridPane);

        signingTab.setOnSelectionChanged(event -> {
            if (signingTab.isSelected()) {
                Platform.runLater(() -> textDataForSigning.requestFocus());
            }
        });

        // Create Encryption tab
        encryptionTab = new Tab();
        encryptionTab.setStyle(BACKGROUND);
        encryptionTab.setText(ENCRYPTION);
        encryptionTab.setTooltip(tooltipEncryptionTab);
        // disabled until keys are created
        encryptionTab.setDisable(true);

        GridPane encryptionPane = new GridPane();
        encryptionPane.setStyle(BACKGROUND);
        encryptionPane.setPadding(new Insets(5));
        encryptionPane.setHgap(5);
        encryptionPane.setVgap(5);
        ColumnConstraints encryptionColumn1 = new ColumnConstraints();
        encryptionColumn1.setPercentWidth(45);
        ColumnConstraints encryptionColumn2 = new ColumnConstraints();
        encryptionColumn2.setPercentWidth(45);
        encryptionPane.getColumnConstraints().addAll(encryptionColumn1, encryptionColumn2);
        setRowConstraints(encryptionPane);
        encryptionPane.add(new Label(""), 0, 0);

        // Data box to enter data into
        Label labelDataToEncrypt = new Label("Plain text");
        encryptionPane.add(labelDataToEncrypt, 0, 1);

        textDataToEncrypt = new TextArea();
        textDataToEncrypt.setWrapText(true);
        encryptionPane.add(textDataToEncrypt, 0, 2, 1, 8);

        // Signature box to show encrypted data
        Label labelDataEncrypted = new Label("Encrypted data");
        encryptionPane.add(labelDataEncrypted, 1, 1);

        textDataEncrypted = new TextArea();
        textDataEncrypted.setWrapText(true);
        textDataEncrypted.setEditable(false);
        encryptionPane.add(textDataEncrypted, 1, 2, 1, 8);

        buttonReEncrypt.setStyle(BUTTON);
        GridPane.setHalignment(buttonReEncrypt, HPos.RIGHT);
        buttonReEncrypt.setVisible(false);
        encryptionPane.add(buttonReEncrypt, 0, 11);

        buttonEncryptionStatus = new Button();
        buttonEncryptionStatus.setMaxWidth(Double.MAX_VALUE);
        buttonEncryptionStatus.setText("Signature is correct!");
        buttonEncryptionStatus.setVisible(false);
        GridPane.setColumnSpan(buttonEncryptionStatus, 2);
        encryptionPane.add(buttonEncryptionStatus, 0, 13);

        encryptionTab.setContent(encryptionPane);

        encryptionTab.setOnSelectionChanged(event -> {
            if (encryptionTab.isSelected()) {
                Platform.runLater(() -> textDataToEncrypt.requestFocus());
            }
        });

        decryptionTab = new Tab();
        decryptionTab.setStyle(BACKGROUND);
        decryptionTab.setText(DECRYPTION);
        decryptionTab.setTooltip(tooltipDecryptionTab);
        // disabled until keys are created
        decryptionTab.setDisable(true);

        GridPane decryptionPane = new GridPane();
        decryptionPane.setStyle(BACKGROUND);
        decryptionPane.setPadding(new Insets(5));
        decryptionPane.setHgap(5);
        decryptionPane.setVgap(5);
        ColumnConstraints decryptionColumn1 = new ColumnConstraints();
        decryptionColumn1.setPercentWidth(45);
        ColumnConstraints decryptionColumn2 = new ColumnConstraints();
        decryptionColumn2.setPercentWidth(45);
        decryptionColumn2.setHgrow(Priority.ALWAYS);
        decryptionPane.getColumnConstraints().addAll(decryptionColumn1, decryptionColumn2);
        setRowConstraints(decryptionPane);
        decryptionPane.add(new Label(""), 0, 0);

        decryptionPane.add(new Label("Encrypted data"), 0, 1);
        textDataToDecrypt = new TextArea();
        textDataToDecrypt.setWrapText(true);
        decryptionPane.add(textDataToDecrypt, 0, 2, 1, 8);

        decryptionPane.add(new Label("Plain text"), 1, 1);
        textDataDecrypted = new TextArea();
        textDataDecrypted.setWrapText(true);
        textDataDecrypted.setEditable(false);
        decryptionPane.add(textDataDecrypted, 1, 2, 1, 8);

        decryptionTab.setContent(decryptionPane);

        buttonReDecrypt.setStyle(BUTTON);
        GridPane.setHalignment(buttonReDecrypt, HPos.RIGHT);
        buttonReDecrypt.setVisible(false);
        decryptionPane.add(buttonReDecrypt, 0, 11);

        buttonDecryptionStatus.setMaxWidth(Double.MAX_VALUE);
        buttonDecryptionStatus.setText("Signature is correct!");
        buttonDecryptionStatus.setVisible(false);
        GridPane.setColumnSpan(buttonDecryptionStatus, 2);
        decryptionPane.add(buttonDecryptionStatus, 0, 13);

        decryptionTab.setOnSelectionChanged(event -> {
            if (decryptionTab.isSelected()) {
                Platform.runLater(() -> textDataToDecrypt.requestFocus());
            }
        });

        // XML signing
        xmlSigningTab = new Tab();
        xmlSigningTab.setStyle(BACKGROUND);
        xmlSigningTab.setText("XML Signing");
        xmlSigningTab.setTooltip(tooltipXMLSigningTab);
        // disabled until keys are created
        xmlSigningTab.setDisable(true);

        GridPane signingFileGridPane = new GridPane();
        signingFileGridPane.setStyle(BACKGROUND);
        signingFileGridPane.setPadding(new Insets(5));
        signingFileGridPane.setHgap(5);
        signingFileGridPane.setVgap(5);
        ColumnConstraints signingColumn1 = new ColumnConstraints();
        signingColumn1.setPercentWidth(45);
        ColumnConstraints signingColumn2 = new ColumnConstraints();
        signingColumn2.setPercentWidth(45);
        signingColumn2.setHgrow(Priority.ALWAYS);
        signingFileGridPane.getColumnConstraints().addAll(signingColumn1, signingColumn2);
        setRowConstraints(signingFileGridPane);

        // Create Signing tab: Document signing area
        signingFileGridPane.add(new Label(""), 0, 0);

        // Document box to enter data into
        Label labelDocumentForSigning = new Label("Data to sign");
        signingFileGridPane.add(labelDocumentForSigning, 0, 1);
        textToDocumentForSigning = new TextArea();
        textToDocumentForSigning.setWrapText(true);
        signingFileGridPane.add(textToDocumentForSigning, 0, 2, 1, 8);

        // Signature box to show signature
        Label labelDocumentSignature = new Label("Signature");
        signingFileGridPane.add(labelDocumentSignature, 1, 1);
        textToDocumentSignature = new TextArea();
        textToDocumentSignature.setWrapText(true);
        textToDocumentSignature.setEditable(false);
        signingFileGridPane.add(textToDocumentSignature, 1, 2, 1, 8);

        Button signDocumentButton = new Button("Sign to XML");
        signDocumentButton.setStyle(BUTTON);
        signDocumentButton.setOnAction(this::handleSignDocument);
        GridPane.setHalignment(signDocumentButton, HPos.RIGHT);
        signingFileGridPane.add(signDocumentButton, 0, 11);

        buttonVerifiedDocumentSignature = new Button();
        buttonVerifiedDocumentSignature.setMaxWidth(Double.MAX_VALUE);
        buttonVerifiedDocumentSignature.setVisible(false);
        buttonVerifiedDocumentSignature.setStyle(BUTTON_SUCCESS_SMALL);
        signingFileGridPane.add(buttonVerifiedDocumentSignature, 1, 10);

        Button verifyDocumentButton = new Button("Verify signature");
        verifyDocumentButton.setStyle(BUTTON);
        verifyDocumentButton.setOnAction(this::handleVerifySigned);
        GridPane.setHalignment(verifyDocumentButton, HPos.RIGHT);
        signingFileGridPane.add(verifyDocumentButton, 1, 11);

        signingFileGridPane.add(new Label("Original data"), 0, 13);
        textFromDocumentForSigning = new TextArea();
        textFromDocumentForSigning.setWrapText(true);
        textFromDocumentForSigning.setEditable(false);
        signingFileGridPane.add(textFromDocumentForSigning, 0, 14, 1, 8);

        // Signature box to show signature
        signingFileGridPane.add(new Label("Signature"), 1, 13);
        textFromDocumentSignature = new TextArea();
        textFromDocumentSignature.setWrapText(true);
        textFromDocumentSignature.setEditable(false);
        signingFileGridPane.add(textFromDocumentSignature, 1, 14, 1, 8);

        Button loadDocumentButton = new Button("Load from XML");
        loadDocumentButton.setStyle(BUTTON);
        loadDocumentButton.setOnAction(this::handleLoadDocument);
        GridPane.setHalignment(loadDocumentButton, HPos.RIGHT);
        signingFileGridPane.add(loadDocumentButton, 0, 23);

        Button verifyFromDocumentButton = new Button("Verify signature");
        verifyFromDocumentButton.setStyle(BUTTON);
        verifyFromDocumentButton.setOnAction(this::handleVerifySignedDocument);
        GridPane.setHalignment(verifyFromDocumentButton, HPos.RIGHT);
        signingFileGridPane.add(verifyFromDocumentButton, 1, 23);

        buttonVerifiedFromDocumentSignature = new Button();
        buttonVerifiedFromDocumentSignature.setMaxWidth(Double.MAX_VALUE);
        buttonVerifiedFromDocumentSignature.setText("Signature is correct!");
        buttonVerifiedFromDocumentSignature.setVisible(false);
        buttonVerifiedFromDocumentSignature.setStyle(BUTTON_SUCCESS_SMALL);
        signingFileGridPane.add(buttonVerifiedFromDocumentSignature, 1, 22);

        xmlSigningTab.setContent(signingFileGridPane);

        xmlSigningTab.setOnSelectionChanged(event -> {
            if (xmlSigningTab.isSelected()) {
                Platform.runLater(() -> textToDocumentForSigning.requestFocus());
            }
        });

        xmlEncryptionTab = new Tab();
        xmlEncryptionTab.setStyle(BACKGROUND);
        xmlEncryptionTab.setText("XML-Enc");
        xmlEncryptionTab.setTooltip(tooltipXMLEncryptionTab);
        // disabled until keys are created
        xmlEncryptionTab.setDisable(true);

        GridPane xmlEncGridPane = new GridPane();
        xmlEncGridPane.setStyle(BACKGROUND);
        xmlEncGridPane.setPadding(new Insets(5));
        xmlEncGridPane.setHgap(5);
        xmlEncGridPane.setVgap(5);
        ColumnConstraints xmlEncColumn1 = new ColumnConstraints();
        xmlEncColumn1.setPercentWidth(45);
        ColumnConstraints xmlEncColumn2 = new ColumnConstraints();
        xmlEncColumn2.setPercentWidth(45);
        xmlEncGridPane.getColumnConstraints().addAll(xmlEncColumn1,
                xmlEncColumn2);
        setRowConstraints(xmlEncGridPane);

        // Create xmlEnc tab
        xmlEncGridPane.add(new Label(""), 0, 0);

        // Data box to enter data into
        xmlEncGridPane.add(new Label("Plain text"), 0, 1);

        textDataForXMLEnc = new TextArea();
        textDataForXMLEnc.setWrapText(true);
        xmlEncGridPane.add(textDataForXMLEnc, 0, 2, 1, 8);

        xmlEncGridPane.add(new Label("Symmetric key encrypted with public key")
                , 1, 1);
        textDataXMLSymmetricEnc = new TextArea();
        textDataXMLSymmetricEnc.setEditable(false);
        textDataXMLSymmetricEnc.setWrapText(true);
        xmlEncGridPane.add(textDataXMLSymmetricEnc, 1, 2, 1, 8);

        // Create buttons
        Button xmlEncryptDocumentButton = new Button("Encrypt to XML");
        xmlEncryptDocumentButton.setStyle(BUTTON);
        xmlEncryptDocumentButton.setOnAction(this::handleXMLEncryptDocument);
        xmlEncGridPane.add(xmlEncryptDocumentButton, 0, 11);
        GridPane.setHalignment(xmlEncryptDocumentButton, HPos.RIGHT);

        xmlEncGridPane.add(new Label("Data encrypted with symmetric key")
                , 1, 13);
        textDataXMLAsymmetricEnc = new TextArea();
        textDataXMLAsymmetricEnc.setEditable(false);
        textDataXMLAsymmetricEnc.setWrapText(true);
        textDataXMLAsymmetricEnc.setWrapText(true);
        xmlEncGridPane.add(textDataXMLAsymmetricEnc, 1, 14, 1, 8);

        xmlEncryptionTab.setContent(xmlEncGridPane);

        xmlEncryptionTab.setOnSelectionChanged(event -> {
            if (xmlEncryptionTab.isSelected()) {
                Platform.runLater(() -> textDataForXMLEnc.requestFocus());
            }
        });

        xmlDecryptionTab = new Tab();
        xmlDecryptionTab.setStyle(BACKGROUND);
        xmlDecryptionTab.setText("XML-Dec");
        xmlDecryptionTab.setTooltip(tooltipXMLDecryptionTab);
        // disabled until keys are created
        xmlDecryptionTab.setDisable(true);

        GridPane xmlDecGridPane = new GridPane();
        xmlDecGridPane.setStyle(BACKGROUND);
        xmlDecGridPane.setPadding(new Insets(5));
        xmlDecGridPane.setHgap(5);
        xmlDecGridPane.setVgap(5);
        ColumnConstraints xmlDecColumn1 = new ColumnConstraints();
        xmlDecColumn1.setPercentWidth(45);
        ColumnConstraints xmlDecColumn2 = new ColumnConstraints();
        xmlDecColumn2.setPercentWidth(45);
        xmlDecGridPane.getColumnConstraints().addAll(xmlDecColumn1,
                xmlDecColumn2);
        setRowConstraints(xmlDecGridPane);

        // Create xmlDec tab
        xmlDecGridPane.add(new Label(""), 0, 0);

        // Data box to enter data into
        xmlDecGridPane.add(new Label("Plain text"), 0, 1);
        textDataForXMLDec = new TextArea();
        textDataForXMLDec.setWrapText(true);
        textDataForXMLDec.setEditable(false);
        xmlDecGridPane.add(textDataForXMLDec, 0, 2, 1, 8);

        textDataXMLSymmetricDec = new TextArea();
        textDataXMLSymmetricDec.setEditable(false);
        textDataXMLSymmetricDec.setWrapText(true);
        xmlDecGridPane.add(textDataXMLSymmetricDec, 1, 2, 1, 8);

        xmlDecGridPane.add(
                new Label("Symmetric key encrypted with public key"), 1, 1);
        textDataXMLSymmetricDec = new TextArea();
        textDataXMLSymmetricDec.setEditable(false);
        textDataXMLSymmetricDec.setWrapText(true);
        xmlDecGridPane.add(textDataXMLSymmetricDec, 1, 2, 1, 8);

        xmlDecGridPane.add(new Label("Data encrypted with symmetric key"), 1,
                13);
        textDataXMLAsymmetricDec = new TextArea();
        textDataXMLAsymmetricDec.setEditable(false);
        textDataXMLAsymmetricDec.setWrapText(true);
        xmlDecGridPane.add(textDataXMLAsymmetricDec, 1, 14, 1, 8);

        Button xmlDecryptDocumentButton = new Button("Decrypt from XML");
        xmlDecryptDocumentButton.setStyle(BUTTON);
        xmlDecryptDocumentButton.setOnAction(this::handleXMLDecryptDocument);
        GridPane.setHalignment(xmlDecryptDocumentButton, HPos.RIGHT);
        xmlDecGridPane.add(xmlDecryptDocumentButton, 0, 11);
        xmlDecryptionTab.setContent(xmlDecGridPane);

        xmlDecryptionTab.setOnSelectionChanged(event -> {
            if (xmlDecryptionTab.isSelected()) {
                Platform.runLater(() -> xmlDecryptDocumentButton.requestFocus());
            }
        });

        Tab symmetricEncTab = new Tab();
        symmetricEncTab.setText("Symmetric-Enc");
        symmetricEncTab.setTooltip(tooltipSymmetricEnc);
        symmetricEncTab.setStyle(BACKGROUND);

        GridPane symmetricEncGridPane = new GridPane();
        symmetricEncGridPane.setStyle(BACKGROUND);
        symmetricEncGridPane.setPadding(new Insets(5));
        symmetricEncGridPane.setHgap(5);
        symmetricEncGridPane.setVgap(5);
        ColumnConstraints symmetricEncColumn1 = new ColumnConstraints();
        symmetricEncColumn1.setPercentWidth(45);
        ColumnConstraints symmetricEncColumn2 = new ColumnConstraints();
        symmetricEncColumn2.setPercentWidth(45);
        symmetricEncColumn2.setHgrow(Priority.ALWAYS);
        symmetricEncGridPane.getColumnConstraints().addAll(symmetricEncColumn1,
                symmetricEncColumn2);
        setRowConstraints(symmetricEncGridPane);
        symmetricEncGridPane.add(new Label("ROT-13 Encryption"), 0, 0);
        symmetricEncGridPane.add(new Label("Plain text"), 0, 1);
        symmetricEncGridPane.add(new Label("ROT-13 encrypted"), 1, 1);

        textROT13From = new TextArea();
        textROT13From.setWrapText(true);
        symmetricEncGridPane.add(textROT13From, 0, 2, 1, 8);

        textROT13To = new TextArea();
        textROT13To.setWrapText(true);
        textROT13To.setEditable(false);
        symmetricEncGridPane.add(textROT13To, 1, 2, 1, 8);

        Separator symmetricEncSeparator = new Separator();
        GridPane.setColumnSpan(symmetricEncSeparator, 2);
        symmetricEncGridPane.add(symmetricEncSeparator, 0, 11, 2, 1);

        symmetricEncGridPane.add(new Label("AES Encryption"), 0, 12);

        HBox hBox = new HBox();
        textAESKeyEncrypt = new TextField();
        HBox.setHgrow(textAESKeyEncrypt, Priority.ALWAYS);
        hBox.getChildren().addAll(new Label("Key: "), textAESKeyEncrypt);
        symmetricEncGridPane.add(hBox, 1, 12);

        symmetricEncGridPane.add(new Label("Plaintext"), 0, 13);
        symmetricEncGridPane.add(new Label("AES encrypted"), 1, 13);

        textAESPlainText = new TextArea();
        textAESPlainText.setWrapText(true);
        symmetricEncGridPane.add(textAESPlainText, 0, 14, 1, 8);

        textAESFromEncrypted = new TextArea();
        textAESFromEncrypted.setWrapText(true);
        textAESFromEncrypted.setEditable(false);
        symmetricEncGridPane.add(textAESFromEncrypted, 1, 14, 1, 8);

        symmetricEncTab.setContent(symmetricEncGridPane);

        symmetricEncTab.setOnSelectionChanged(event -> {
            if (symmetricEncTab.isSelected()) {
                Platform.runLater(() -> textROT13From.requestFocus());
            }
        });

        Tab symmetricDecTab = new Tab();
        symmetricDecTab.setText("Symmetric-Dec");
        symmetricDecTab.setStyle(BACKGROUND);
        symmetricDecTab.setTooltip(tooltipSymmetricDec);

        GridPane symmetricDecGridPane = new GridPane();
        symmetricDecGridPane.setStyle(BACKGROUND);
        symmetricDecGridPane.setPadding(new Insets(5));
        symmetricDecGridPane.setHgap(5);
        symmetricDecGridPane.setVgap(5);
        ColumnConstraints symmetricDecColumn1 = new ColumnConstraints();
        symmetricDecColumn1.setPercentWidth(45);
        ColumnConstraints symmetricDecColumn2 = new ColumnConstraints();
        symmetricDecColumn2.setPercentWidth(45);
        symmetricDecColumn2.setHgrow(Priority.ALWAYS);
        symmetricDecGridPane.getColumnConstraints().addAll(symmetricDecColumn1,
                symmetricDecColumn2);
        setRowConstraints(symmetricDecGridPane);
        symmetricDecGridPane.add(new Label("ROT-13 Decryption"), 0, 0);
        symmetricDecGridPane.add(new Label("ROT-13 encrypted"), 0, 1);

        textROT13Encrypted = new TextArea();
        textROT13Encrypted.setWrapText(true);
        symmetricDecGridPane.add(textROT13Encrypted, 0, 2, 1, 8);

        symmetricDecGridPane.add(new Label("Plaintext"), 1, 1);
        textROT13Decrypted = new TextArea();
        textROT13Decrypted.setWrapText(true);
        textROT13Decrypted.setEditable(false);
        symmetricDecGridPane.add(textROT13Decrypted, 1, 2, 1, 8);

        Separator symmetricDecSeparator = new Separator();
        GridPane.setColumnSpan(symmetricDecSeparator, 2);
        symmetricDecGridPane.add(symmetricDecSeparator, 0, 11, 2, 1);

        symmetricDecGridPane.add(new Label("AES Decryption"), 0, 12);

        HBox hBoxAESDecrypt = new HBox();
        textAESKeyDecrypt = new TextField();
        HBox.setHgrow(textAESKeyDecrypt, Priority.ALWAYS);
        hBoxAESDecrypt.getChildren().addAll(new Label("Key: "), textAESKeyDecrypt);
        symmetricDecGridPane.add(hBoxAESDecrypt, 1, 12);

        symmetricDecGridPane.add(new Label("AES encrypted"), 0, 13);
        symmetricDecGridPane.add(new Label("Plaintext"), 1, 13);

        textAESEncrypted = new TextArea();
        textAESEncrypted.setWrapText(true);
        symmetricDecGridPane.add(textAESEncrypted, 0, 14, 1, 8);

        textAESFDecrypted = new TextArea();
        textAESFDecrypted.setEditable(false);
        textAESFDecrypted.setWrapText(true);
        symmetricDecGridPane.add(textAESFDecrypted, 1, 14, 1, 8);

        symmetricDecTab.setContent(symmetricDecGridPane);
        symmetricDecTab.setOnSelectionChanged(event -> {
            if (symmetricDecTab.isSelected()) {
                Platform.runLater(() -> textROT13Encrypted.requestFocus());
            }
        });

        Tab checksumTab = new Tab();
        checksumTab.setStyle(BACKGROUND);
        checksumTab.setText("Checksums");
        checksumTab.setTooltip(tooltipChecksumTab);

        GridPane checksumGridPane = new GridPane();
        checksumGridPane.setStyle(BACKGROUND);
        checksumGridPane.setPadding(new Insets(5));
        checksumGridPane.setHgap(5);
        checksumGridPane.setVgap(5);
        ColumnConstraints checksumColumn1 = new ColumnConstraints();
        checksumColumn1.setPercentWidth(45);
        ColumnConstraints checksumColumn2 = new ColumnConstraints();
        checksumColumn2.setPercentWidth(45);
        checksumGridPane.getColumnConstraints().addAll(checksumColumn1, checksumColumn2);

        setRowConstraints(checksumGridPane);

        checksumGridPane.add(new Label("Data and file checksums"), 0, 0);
        checksumGridPane.add(new Label("Data to checksum"), 0, 1);
        textDataForChecksum = new TextArea();
        textDataForChecksum.setWrapText(true);
        checksumGridPane.add(textDataForChecksum, 0, 2, 1, 3);

        Separator checksumSeparator = new Separator();
        checksumGridPane.add(checksumSeparator, 0, 5);

        checksumGridPane.add(new Label("File location"), 0, 6);
        textFileLocationForChecksum = new TextField();
        textFileLocationForChecksum.setEditable(false);
        checksumGridPane.add(textFileLocationForChecksum, 0, 7);

        Button generateChecksumFile = new Button("Load file to checksum");
        generateChecksumFile.setStyle(BUTTON);
        generateChecksumFile.setOnAction(this::handleGenerateChecksumFile);
        GridPane.setHalignment(generateChecksumFile, HPos.RIGHT);
        checksumGridPane.add(generateChecksumFile, 0, 9);

        checksumGridPane.add(new Label("md5"), 1, 1);
        textMd5 = new TextField();
        textMd5.setEditable(false);
        checksumGridPane.add(textMd5, 1, 2);

        checksumGridPane.add(new Label("sha-1"), 1, 3);
        textSha1 = new TextField();
        textSha1.setEditable(false);
        checksumGridPane.add(textSha1, 1, 4);

        checksumGridPane.add(new Label("sha-256"), 1, 5);
        textSha256 = new TextArea();
        textSha256.setWrapText(true);
        textSha256.setEditable(false);
        checksumGridPane.add(textSha256, 1, 6, 1, 2);

        checksumGridPane.add(new Label("sha-512"), 1, 8);
        textSha512 = new TextArea();
        textSha512.setWrapText(true);
        textSha512.setEditable(false);
        checksumGridPane.add(textSha512, 1, 9, 1, 3);

        checksumTab.setContent(checksumGridPane);

        checksumTab.setOnSelectionChanged(event -> {
            if (checksumTab.isSelected()) {
                Platform.runLater(() -> textDataForChecksum.requestFocus());
            }
        });

        // Add Base64

        Tab base64Tab = new Tab();
        base64Tab.setStyle(BACKGROUND);
        base64Tab.setText("Base64");
        base64Tab.setTooltip(tooltipBase64Tab);

        GridPane base64GridPane = new GridPane();
        base64GridPane.setStyle(BACKGROUND);
        base64GridPane.setPadding(new Insets(5));
        base64GridPane.setHgap(5);
        base64GridPane.setVgap(5);
        ColumnConstraints base64Column1 = new ColumnConstraints();
        base64Column1.setPercentWidth(45);
        ColumnConstraints base64Column2 = new ColumnConstraints();
        base64Column2.setPercentWidth(45);
        base64Column2.setHgrow(Priority.ALWAYS);
        base64GridPane.getColumnConstraints().addAll(base64Column1,
                base64Column2);
        setRowConstraints(base64GridPane);
        base64GridPane.add(new Label("Base64 encoding"), 0, 0);
        base64GridPane.add(new Label("Plain text"), 0, 1);
        base64GridPane.add(new Label("Base64 encoded"), 1, 1);

        textBase64From = new TextArea();
        textBase64From.setWrapText(true);
        base64GridPane.add(textBase64From, 0, 2, 1, 8);

        textBase64Encoded = new TextArea();
        textBase64Encoded.setWrapText(true);
        textBase64Encoded.setEditable(false);
        base64GridPane.add(textBase64Encoded, 1, 2, 1, 8);

        Separator base64Separator = new Separator();
        base64GridPane.add(base64Separator, 0, 11, 2, 1);

        base64GridPane.add(new Label("Base64 decoding"), 0, 12);
        base64GridPane.add(new Label("Base64 encoded"), 0, 13);
        base64GridPane.add(new Label("Plain text"), 1, 13);

        textBase64EncodedFrom = new TextArea();
        textBase64EncodedFrom.setWrapText(true);
        base64GridPane.add(textBase64EncodedFrom, 0, 14, 1, 8);

        textBase64Decoded = new TextArea();
        textBase64Decoded.setWrapText(true);
        textBase64Decoded.setEditable(false);
        base64GridPane.add(textBase64Decoded, 1, 14, 1, 8);
        base64Tab.setContent(base64GridPane);

        base64Tab.setOnSelectionChanged(event -> {
            if (base64Tab.isSelected()) {
                Platform.runLater(() -> textBase64From.requestFocus());
            }
        });

        // Add UUID
        Tab uuidTab = new Tab();
        uuidTab.setStyle(BACKGROUND);
        uuidTab.setText("UUID");
        uuidTab.setTooltip(tooltipUUIDTab);

        GridPane uuidGridPane = new GridPane();
        uuidGridPane.setStyle(BACKGROUND);
        uuidGridPane.setPadding(new Insets(5));
        uuidGridPane.setHgap(5);
        uuidGridPane.setVgap(5);
        ColumnConstraints uuidColumn1 = new ColumnConstraints();
        uuidColumn1.setPercentWidth(45);
        ColumnConstraints uuidColumn2 = new ColumnConstraints();
        uuidColumn2.setPercentWidth(45);
        uuidGridPane.getColumnConstraints().addAll(uuidColumn1,
                uuidColumn2);
        setRowConstraints(uuidGridPane);

        ObservableList<String> howManyOptions =
                FXCollections.observableArrayList("1", "10", "20", "100");

        howManyComboBox = new ComboBox<>(howManyOptions);
        howManyComboBox.setValue("1");
        uuidGridPane.add(howManyComboBox, 0, 1);

        Button generateUUIDButton = new Button("Generate UUID");
        generateUUIDButton.setStyle(BUTTON);
        generateUUIDButton.setOnAction(this::handleGenerateUUID);
        uuidGridPane.add(generateUUIDButton, 0, 4);

        textUUID = new TextArea();
        textUUID.setEditable(false);
        textUUID.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        uuidGridPane.add(textUUID, 1, 1, 1, 21);

        uuidTab.setContent(uuidGridPane);

        uuidTab.setOnSelectionChanged(event -> {
            if (uuidTab.isSelected()) {
                Platform.runLater(() -> howManyComboBox.requestFocus());
            }
        });

        operationsPane.getTabs().add(keysTab);
        operationsPane.getTabs().add(signingTab);
        operationsPane.getTabs().add(encryptionTab);
        operationsPane.getTabs().add(decryptionTab);
        operationsPane.getTabs().add(xmlSigningTab);
        operationsPane.getTabs().add(xmlEncryptionTab);
        operationsPane.getTabs().add(xmlDecryptionTab);
        operationsPane.getTabs().add(symmetricEncTab);
        operationsPane.getTabs().add(symmetricDecTab);
        operationsPane.getTabs().add(checksumTab);
        operationsPane.getTabs().add(base64Tab);
        operationsPane.getTabs().add(uuidTab);

        operationsPanel.getChildren().addAll(operationsPane);
        layout.setCenter(operationsPanel);

        statusBar = new Label();
        layout.setBottom(statusBar);

        Scene appScene = new Scene(layout, 1360, 768);
        appScene.getStylesheets().add("bootstrap3.css");

        final KeyCombination ctrlLeft =
                new KeyCodeCombination(
                        KeyCode.LEFT, KeyCombination.CONTROL_DOWN);

        final KeyCombination ctrlRight =
                new KeyCodeCombination(
                        KeyCode.RIGHT, KeyCombination.CONTROL_DOWN);

        final KeyCombination commandLeft =
                new KeyCodeCombination(
                        KeyCode.LEFT, KeyCombination.META_DOWN);

        final KeyCombination commandRight =
                new KeyCodeCombination(
                        KeyCode.RIGHT, KeyCombination.META_DOWN);

        final KeyCombination ctrlG =
                new KeyCodeCombination(
                        KeyCode.G, KeyCombination.CONTROL_DOWN);

        final KeyCombination commandG =
                new KeyCodeCombination(
                        KeyCode.G, KeyCombination.META_DOWN);

        final KeyCombination ctrlL =
                new KeyCodeCombination(
                        KeyCode.L, KeyCombination.CONTROL_DOWN);

        final KeyCombination commandL =
                new KeyCodeCombination(
                        KeyCode.L, KeyCombination.META_DOWN);

        final KeyCombination ctrlO =
                new KeyCodeCombination(
                        KeyCode.O, KeyCombination.CONTROL_DOWN);

        final KeyCombination commandO =
                new KeyCodeCombination(
                        KeyCode.O, KeyCombination.META_DOWN);

        final KeyCombination ctrlU =
                new KeyCodeCombination(
                        KeyCode.U, KeyCombination.CONTROL_DOWN);

        final KeyCombination commandU =
                new KeyCodeCombination(
                        KeyCode.U, KeyCombination.META_DOWN);

        final KeyCombination ctrlB =
                new KeyCodeCombination(
                        KeyCode.B, KeyCombination.CONTROL_DOWN);

        final KeyCombination commandB =
                new KeyCodeCombination(
                        KeyCode.B, KeyCombination.META_DOWN);

        appScene.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
            if (ctrlLeft.match(event) || commandLeft.match(event)) {
                selectLeftTab();
            }
            if (ctrlRight.match(event) || commandRight.match(event)) {
                selectRightTab();
            }
            if (ctrlG.match(event) || commandG.match(event)) {
                generateKeyPair();
            }
            if (ctrlL.match(event) || commandL.match(event)) {
                loadKeyPair();
            }
            if (ctrlU.match(event) || commandU.match(event)) {
                loadPublicKey();
            }
            if (ctrlO.match(event) || commandO.match(event)) {
                viewLog();
            }
            if (ctrlB.match(event) || commandB.match(event)) {
                viewAbout();
            }
        });

        textDataToDecrypt.textProperty().addListener((observable,
                                                      oldValue, newValue) -> {
            buttonDecryptionStatus.setVisible(false);
        });

        /*
         *  copy contents of textSha256 every time the value changes
         *
         *  Helper functionality for OsloMet students. They often need to
         *  generate sha-256 checksums. This just makes it easy as they
         *  automatically get the value copied to the clipboard
         */
        textSha256.textProperty().addListener((observable,
                                               oldValue, newValue) -> {
            copyToClipboard(textSha256.getText());
        });

        /*
         *  copy contents of textBase64Encoded every time the value changes
         *
         *  You probably need to copy the newly created base64 encoded value
         *  so this is just a helper to save time.
         */
        textBase64Encoded.textProperty().addListener((observable,
                                                      oldValue, newValue) -> {
            copyToClipboard(textBase64Encoded.getText());
        });

        buttonReSign.setOnAction(event -> {
            signData();
            buttonReSign.setVisible(false);
        });

        buttonReEncrypt.setOnAction(event -> {
            encryptData();
            buttonReEncrypt.setVisible(false);
        });

        buttonReDecrypt.setOnAction(event -> {
            buttonDecryptionStatus.setVisible(false);
            decryptData();
            buttonReDecrypt.setVisible(false);
        });

        textDataForChecksum.textProperty().addListener((observable,
                                                        oldValue, newValue) -> {
            if (textDataForChecksum.getText().equals("")) {
                textMd5.setText("");
                textSha1.setText("");
                textSha256.setText("");
                textSha512.setText("");
            } else {
                try {
                    textFileLocationForChecksum.setText("");
                    byte[] textToChecksum = textDataForChecksum.getText()
                            .getBytes();
                    // Get the MD5 checksum
                    textMd5.setText(DatatypeConverter.printHexBinary(
                            MessageDigest.getInstance("MD5").
                                    digest(textToChecksum)).toUpperCase());

                    // Get the MD5 checksum
                    textSha1.setText(DatatypeConverter.printHexBinary(
                            MessageDigest.getInstance(SHA_1).
                                    digest(textToChecksum)).toUpperCase());

                    // Get the sha-256 checksum
                    textSha256.setText(DatatypeConverter.printHexBinary(
                            MessageDigest.getInstance("SHA-256").
                                    digest(textToChecksum)).toUpperCase());

                    // Get the sha-512 checksum
                    textSha512.setText(DatatypeConverter.printHexBinary(
                            MessageDigest.getInstance("SHA-512").
                                    digest(textToChecksum)).toUpperCase());
                    logEvent("Generated checksums for data");
                } catch (NoSuchAlgorithmException e) {
                    logEvent(e.toString());
                    // e.printStackTrace();
                }
            }
        });

        textBase64From.textProperty().addListener((observable,
                                                   oldValue, newValue) -> {
            textBase64Encoded.setText(Base64.getEncoder().
                    encodeToString(textBase64From.getText().getBytes(UTF_8)));
            logEvent("Converted text from plain text to Base64");
        });

        textBase64EncodedFrom.textProperty().addListener((observable,
                                                          oldValue, newValue) -> {
            textBase64Decoded.setText(new String(Base64.getDecoder().
                    decode(textBase64EncodedFrom.getText()), UTF_8));
            logEvent("Converted text from Base64 to plain text");
        });

        textAESPlainText.textProperty().addListener((observable,
                                                     oldValue, newValue) -> {
            if (!textAESKeyEncrypt.getText().equals("")) {
                handleEncryptWithAES();
                copyToClipboard(textAESFromEncrypted.getText());
            } else {
                textAESFromEncrypted.setText("");
            }
            if (textAESPlainText.getText().equals("")) {
                textAESFromEncrypted.setText("");
            }
        });

        textAESEncrypted.textProperty().addListener((observable,
                                                     oldValue, newValue) -> {
            handleDecryptWithAES();
        });

        textAESKeyEncrypt.textProperty().addListener((observable,
                                                      oldValue, newValue) -> {
            if (!textAESPlainText.getText().equals("")) {
                handleEncryptWithAES();
                copyToClipboard(textAESFromEncrypted.getText());
            } else {
                textAESFromEncrypted.setText("");
            }
            if (textAESKeyEncrypt.getText().equals("")) {
                textAESFromEncrypted.setText("");
            }
        });

        textDataEncrypted.textProperty().addListener((observable,
                                                      oldValue, newValue) -> {
            copyToClipboard(textDataEncrypted.getText());
        });

        textAESKeyDecrypt.textProperty().addListener((observable,
                                                      oldValue, newValue) -> {
            handleDecryptWithAES();
        });

        textROT13Encrypted.textProperty().addListener((observable,
                                                       oldValue, newValue) -> {
            textROT13Decrypted.setText(doROT13(textROT13Encrypted.getText()));
            logEvent("Decrypted data using ROT-13");
        });

        textROT13From.textProperty().addListener((observable,
                                                  oldValue, newValue) -> {
            textROT13To.setText(doROT13(textROT13From.getText()));
            copyToClipboard(textROT13To.getText());
            logEvent("Encrypted data using ROT-13");
        });


        textToDocumentForSigning.textProperty().addListener((observable,
                                                             oldValue, newValue) -> {
            if (newValue.equals("")) {
                textToDocumentSignature.setText("");
            }
        });

        /*
         * If there is any change in textDataForXMLEnc, empty the
         * corresponding TextArea. This is because the signatures will not
         * match any more.
         */
        textDataForXMLEnc.textProperty().addListener((observable,
                                                      oldValue, newValue) -> {
            textDataXMLAsymmetricEnc.setText("");
            textDataXMLSymmetricEnc.setText("");
        });

        textDataToEncrypt.textProperty().addListener((observable,
                                                      oldValue, newValue) -> {
            if (newValue.equals("")) {
                textDataEncrypted.setText("");
                return;
            }
            encryptData();
        });

        textDataToDecrypt.textProperty().addListener((observable,
                                                      oldValue, newValue) -> {
            if (textDataToDecrypt.getText().equals("")) {
                return;
            }
            decryptData();
        });


        textDataForSigning.textProperty().addListener((observable,
                                                       oldValue, newValue) -> {
            if (newValue.equals("")) {
                textDataSignature.setText("");
                return;
            }
            signData();
        });

        stage.getIcons().add(new Image(ChestnutApplication.class.
                getClassLoader().getResource("images/appicon.png")
                .toExternalForm()));

        stage.setScene(appScene);
        stage.show();
        logEvent("Application started");
    }


    private void signData() {
        try {
            buttonVerifiedSignature.setVisible(false);
            String key = getSelectedKeyIdentifier();
            if (key != null) {
                String keyType = keyController.getKeyType(key);
                if (keyType.equals(PRIVATE_PUBLIC_KEYPAIR)) {
                    String data = textDataForSigning.getText();
                    textDataSignature.setText(signatureController.createSignature(data,
                            key, SIGN_SHA_256_RSA));
                } else {
                    logEvent("You can only sign with a private key!");
                }
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException |
                SignatureException e) {
            logEvent(e.toString());
            e.printStackTrace();
        }

    }

    private void encryptData() {
        String key = getSelectedKeyIdentifier();
        try {
            if (key != null) {
                String data = textDataToEncrypt.getText();
                textDataEncrypted.setText(encryptionController.
                        encryptDataWithPublicKey(key, data, ENCRYPT_RSA_DEFAULT));
                buttonEncryptionStatus.setVisible(true);
                buttonEncryptionStatus.setText("Encryption successful!");
                buttonEncryptionStatus.setStyle(BUTTON_SUCCESS);
                logEvent("Encrypted data using public key identified by ("
                        + key + ")");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException |
                NoSuchPaddingException | IllegalBlockSizeException |
                BadPaddingException e) {
            buttonEncryptionStatus.setVisible(true);
            buttonEncryptionStatus.setText("Encryption unsuccessful!");
            buttonEncryptionStatus.setStyle(BUTTON_DANGER);
            logEvent("Could not encrypt data using private key identified" +
                    " by (" + key + ")");
            e.printStackTrace();
        }
    }

    private void decryptData() {
        textDataDecrypted.setText("");
        String key = getSelectedKeyIdentifier();
        try {
            if (key != null) {
                String keyType = keyController.getKeyType(key);
                if (keyType.equals(PRIVATE_PUBLIC_KEYPAIR)) {
                    String data = textDataToDecrypt.getText();
                    String decryptedText = decryptionController.
                            decryptData(data, key, ENCRYPT_RSA_DEFAULT);
                    textDataDecrypted.setText(decryptedText);
                    buttonDecryptionStatus.setVisible(true);
                    buttonDecryptionStatus.setText("Decryption successful!");
                    buttonDecryptionStatus.setStyle(BUTTON_SUCCESS);
                    logEvent("Decrypted data using private key identified by ("
                            + key + ")");
                } else {
                    logEvent("You can only decrypt data with a private key! " +
                            "Select a private key to decrypt data with");
                    buttonDecryptionStatus.setVisible(true);
                    buttonDecryptionStatus.setText("Decryption unsuccessful!");
                    buttonDecryptionStatus.setStyle(BUTTON_DANGER);
                }
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException |
                NoSuchPaddingException | IllegalBlockSizeException |
                BadPaddingException | IllegalArgumentException e) {
            buttonDecryptionStatus.setText("Decryption unsuccessful!");
            buttonDecryptionStatus.setVisible(true);
            buttonDecryptionStatus.setStyle(BUTTON_DANGER);
            logEvent("Could not decrypt data using private key identified" +
                    " by (" + key + ")");
            e.printStackTrace();
        }
    }

    private String doROT13(String data) {
        char[] input = data.toCharArray();
        StringBuilder rotated = new StringBuilder();
        for (char anInput : input) {
            int current = (int) anInput;
            if ((int) anInput >= 97 && (int) anInput <= 122) {
                current = ((int) anInput - 97 + 13) % 26 + 97;
            } else if ((int) anInput >= 65 && (int) anInput <= 90) {
                current = ((int) anInput - 65 + 13) % 26 + 65;
            }
            rotated.append((char) current);
        }
        return rotated.toString();
    }

    private void handleEncryptWithAES() {
        try {

            if (!textAESPlainText.getText().equals("")) {

                byte[] digest = MessageDigest.getInstance("SHA-1").
                        digest(textAESKeyEncrypt.getText().getBytes(UTF_8));
                byte[] key = Arrays.copyOf(digest, 16);
                SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                Cipher cipher = Cipher.getInstance(ENCRYPT_AES_DEFAULT);
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
                textAESFromEncrypted.setText(Base64.getEncoder().encodeToString(cipher
                        .doFinal(textAESPlainText.getText().getBytes(UTF_8))));
                logEvent("Encrypted data using AES with key (" +
                        textAESKeyEncrypt.getText() + ")");
            }
        } catch (BadPaddingException |
                InvalidKeyException | IllegalBlockSizeException |
                NoSuchPaddingException | NoSuchAlgorithmException e) {
            logEvent("Could not encrypt data using AES and key (" +
                    textAESKeyDecrypt.getText() + ")");
            e.printStackTrace();
        }
    }

    private void handleDecryptWithAES() {
        try {

            if (!textAESEncrypted.getText().equals("")) {

                byte[] digest = MessageDigest.getInstance(SHA_1).
                        digest(textAESKeyDecrypt.getText().getBytes(UTF_8));
                byte[] key = Arrays.copyOf(digest, 16);
                SecretKeySpec secretKey = new SecretKeySpec(key, SYMMETRIC_ALGORITHM);
                Cipher cipher = Cipher.getInstance(ENCRYPT_AES_DEFAULT);
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                textAESFDecrypted.setText(
                        new String(cipher.doFinal(
                                Base64.getDecoder().decode(textAESEncrypted.getText()
                                        .getBytes(UTF_8))), UTF_8));
                logEvent("Decrypted data using AES with key (" +
                        textAESKeyDecrypt.getText() + ")");
            }
        } catch (BadPaddingException | InvalidKeyException |
                IllegalBlockSizeException | NoSuchPaddingException |
                NoSuchAlgorithmException e) {
            logEvent("Could not decrypt data using AES and key (" +
                    textAESKeyDecrypt.getText() + ")");
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void handleVerifySignedData(ActionEvent event) {
        try {
            String key = getSelectedKeyIdentifier();
            String data = textDataForSigning.getText();
            String signature = textDataSignature.getText();
            boolean verified = signatureController.
                    verifySignatureEncodedBase64(data, signature, key,
                            SIGN_SHA_256_RSA);

            if (verified) {
                buttonVerifiedSignature.setText("Signature is correct!");
                buttonVerifiedSignature.setVisible(true);
                buttonVerifiedSignature.setStyle(BUTTON_SUCCESS);
                logEvent("Verified data using key (" + key + ")");
            } else {
                buttonVerifiedSignature.setText("Signature is incorrect!");
                buttonVerifiedSignature.setVisible(true);
                buttonVerifiedSignature.setStyle(BUTTON_DANGER);
                logEvent("Could not verify data using key (" + key + ")");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException |
                SignatureException e) {
            logEvent(e.toString());
            if (e instanceof SignatureException) {
                logEvent("Could not verify signature using given key!" + System
                        .lineSeparator());
                buttonVerifiedSignature.setText("Could not verify signature!");
                buttonVerifiedSignature.setVisible(true);
                buttonVerifiedSignature.setStyle(BUTTON_DANGER);
            }
            e.printStackTrace();
        }
    }


    private String getSelectedKeyIdentifier() {

        Label lkeyPair = ownKeyList.getSelectionModel().getSelectedItem();
        Label lpublicKeyPair = publicKeyList.getSelectionModel()
                .getSelectedItem();

        if (lkeyPair == null && lpublicKeyPair == null) {

            Dialog<Pair<String, String>> dialog = new Dialog<>();
            dialog.setTitle("chestnut");
            dialog.setHeaderText("No key selected. Select key to sign!");
            dialog.getDialogPane().getButtonTypes().addAll(
                    ButtonType.OK);
            dialog.showAndWait();
            return null;
        }
        String key = null;
        if (lkeyPair != null) {
            key = lkeyPair.getText();
        }
        if (lpublicKeyPair != null) {
            key = lpublicKeyPair.getText();
        }
        return key;
    }

    @SuppressWarnings("unused")
    private void handleGenerateUUID(ActionEvent event) {

        StringBuilder uuids = new StringBuilder();

        int howMany = Integer.valueOf(howManyComboBox.
                getSelectionModel().getSelectedItem());

        for (int i = 0; i < howMany; i++) {
            uuids.append(UUID.randomUUID().toString()).
                    append(System.lineSeparator());
        }
        String uuidAsString = uuids.toString();
        copyToClipboard(uuidAsString);
        textUUID.setText(uuidAsString);
        logEvent("Generated " + howMany + " UUIDs");
    }

    @SuppressWarnings("unused")
    private void handleSavePrivateKey(ActionEvent event) {
        Label lkeyPair = ownKeyList.getSelectionModel().getSelectedItem();

        if (lkeyPair != null) {
            PrivateKey privateKey = keyController.getPrivateKey(lkeyPair.getText());

            File selectedFile = loadSaveDialog("Save public key",
                    lkeyPair.getText() + ".key",
                    "Private key file", "*.key");

            if (selectedFile != null) {
                try {
                    keyController.savePrivateKeyToDisk(
                            new FileWriter(selectedFile), privateKey);
                    logEvent("Saved private key identified with (" + lkeyPair
                            .getText() + ") to [" +
                            selectedFile.getAbsolutePath() + "]");
                } catch (IOException e) {
                    logEvent("Could not saved private key identified with (" +
                            lkeyPair.getText() + ") to [" +
                            selectedFile.getAbsolutePath() + "]");
                    logEvent(e.toString());
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressWarnings("unused")
    private void handleSavePublicKey(ActionEvent event) {
        Label lkeyPair = ownKeyList.getSelectionModel().getSelectedItem();
        Label lpublicKeyPair = publicKeyList.getSelectionModel()
                .getSelectedItem();
        String key = null;
        if (lkeyPair != null) {
            key = lkeyPair.getText();
        }
        if (lpublicKeyPair != null) {
            key = lpublicKeyPair.getText();
        }
        String filename = "";
        if (key != null) {
            filename = key + ".pub";
        }

        PublicKey publicKey = keyController.getPublicKey(key);
        File selectedFile = loadSaveDialog("Save public key", filename,
                "Public key file", "*.pub");

        if (selectedFile != null) {
            try {
                keyController.savePublicKeyToDisk(
                        new FileWriter(selectedFile), publicKey);
                logEvent("Saved public key identified with (" + key + ") to " +
                        "[" + selectedFile.getAbsolutePath() + "]");
            } catch (IOException e) {
                logEvent("Could not save public key identified with (" + key +
                        ") to [" + selectedFile.getAbsolutePath() + "]");
                logEvent(e.toString());
                e.printStackTrace();
            }
        }
    }


    private File loadSaveDialog(String title, String filename, String description,
                                String extension) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser.setInitialFileName(filename);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.
                        ExtensionFilter(description, extension));

        // Try to set the users home directory as starting point
        String homeDirectory = getHome();
        File testHomeDirectory = new File(homeDirectory);
        if (testHomeDirectory.exists() && !testHomeDirectory.isDirectory()) {
            fileChooser.setInitialDirectory(new File(homeDirectory));
        }

        return fileChooser.showSaveDialog(stage);
    }

    private File loadOpenDialog(String title, String description,
                                String extension) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.
                        ExtensionFilter(description, extension));

        // Try to set the users home directory as starting point
        String homeDirectory = getHome();
        File testHomeDirectory = new File(homeDirectory);
        if (testHomeDirectory.exists() && !testHomeDirectory.isDirectory()) {
            fileChooser.setInitialDirectory(new File(homeDirectory));
        }

        File selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile == null) {
            // Nothing was selected, just return
            return null;
        }

        if (!selectedFile.canRead()) {
            //Pop up alert saying you cannot read this file
            return null;
        }
        return selectedFile;
    }

    @SuppressWarnings("unused")
    private void handleLoadPublicKey(ActionEvent event) {
        loadPublicKey();
    }

    private void loadPublicKey() {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select public key");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.
                        ExtensionFilter("Public key Files", "*.pub"));

        // Try to set the users home directory as starting point
        String homeDirectory = getHome();
        File testHomeDirectory = new File(homeDirectory);
        if (testHomeDirectory.exists() && !testHomeDirectory.isDirectory()) {
            fileChooser.setInitialDirectory(new File(homeDirectory));
        }

        File selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile == null) {
            // Nothing was selected, just return
            return;
        }

        if (!selectedFile.canRead()) {
            //Pop up alert saying you cannot read this file
        }
        String name = selectedFile.getName();
        String index = name.substring(0,
                name.lastIndexOf('.'));
        try {
            if (!keyController.
                    loadPublicKeyFromDisk(index, selectedFile.toPath(), RSA)) {
                logEvent("Could not import public key. Does the file have the " +
                        "same name (" + index + ") as an existing public key or "
                        + "keypair?");
                return;
            }

            publicKeyList.getItems().add(new Label(index));
            int size = publicKeyList.getItems().size();
            publicKeyList.getSelectionModel().select(size - 1);
            setSelectedPublicKey();
            enableTabsThatRequireKeys();
            logEvent("Loaded public key from [" + selectedFile
                    .getAbsolutePath() + "] identified as (" + index + ")");
        } catch (IOException | NoSuchAlgorithmException |
                InvalidKeySpecException e) {
            logEvent(e.toString());
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void handleLoadKeyPair(ActionEvent event) {
        loadKeyPair();
    }

    private void loadKeyPair() {
        final FileChooser fileChooser = new FileChooser();

        List<File> list =
                fileChooser.showOpenMultipleDialog(stage);
        if (list.size() != 2) {
            logEvent("You must choose 2 files in a keypair. A public key and" +
                    "a private key");
            return;
        }

        StringBuilder keyA = new StringBuilder();
        StringBuilder keyB = new StringBuilder();

        File a = list.get(0);
        File b = list.get(1);
        String keyTypeA = keyController.getKeyAndKeyType(a.toPath(), keyA);
        String keyTypeB = keyController.getKeyAndKeyType(b.toPath(), keyB);

        if (keyTypeA == null || keyTypeB == null) {
            logEvent("Files are not a private/public key keypair!");
            return;
        }
        if (keyTypeA.equals(keyTypeB)) {
            logEvent("Files are not a private/public key keypair!");
            return;
        }

        // Choose the index from from private key filename
        String name = keyTypeA.equals(PRIVATE) ? a.getName() : b.getName();
        String index = name.substring(0, name.lastIndexOf('.'));

        StringBuilder publicKey = keyTypeA.equals(PUBLIC) ? keyA : keyB;
        StringBuilder privateKey = keyTypeA.equals(PRIVATE) ? keyA : keyB;

        try {

            if (!keyController.addKeys(
                    privateKey.toString(),
                    publicKey.toString(),
                    RSA,
                    index)) {
                logEvent("Could not import keys. Does the file have the same " +
                        "name (" + index + ") as an existing public key or" +
                        " keypair?");
                return;
            }

            ownKeyList.getItems().add(new Label(index));
            int size = ownKeyList.getItems().size();
            ownKeyList.getSelectionModel().select(size - 1);
            setSelectedKeyPair();
            enableTabsThatRequireKeys();
            logEvent("Import keys identified by (" + index + "). Keys loaded " +
                    "from file [" + a.getAbsolutePath() + "]");
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            logEvent("Error loading key pair " + e.toString());
        }

    }

    /*
     *  The following methods work together to ensure that we know which key
     *  to use. If a user has selected both a oen key pair and public key
     *  then we would have to ask the user which one to use. By doing this means
     *  we can only have one selected key and that's the last one to be
     *  selected. The user will not notice this but internal functionality
     *  here needs it.
     */
    @SuppressWarnings("unused")
    private void handlePublicKeysPanelSelect(MouseEvent event) {
        ownKeyList.getSelectionModel().clearSelection();
        clearKeysValues();
        setSelectedPublicKey();
    }

    @SuppressWarnings("unused")
    private void handleOwnKeysPanelSelect(MouseEvent event) {
        publicKeyList.getSelectionModel().clearSelection();
        clearKeysValues();
        setSelectedKeyPair();
    }

    @SuppressWarnings("unused")
    private void handlePublicKeysPanelSelect(KeyEvent event) {
        ownKeyList.getSelectionModel().clearSelection();
        clearKeysValues();
    }

    @SuppressWarnings("unused")
    private void handleOwnKeysPanelSelect(KeyEvent event) {
        publicKeyList.getSelectionModel().clearSelection();
        clearKeysValues();
        setSelectedKeyPair();
    }

    private void clearKeysValues() {
        textKeyType.setText("");
        textPrivateKey.setText("");
        textPublicKey.setText("");

        textDataDecrypted.setText("");
        textDataEncrypted.setText("");
        textDataSignature.setText("");
        buttonDecryptionStatus.setVisible(false);
        buttonEncryptionStatus.setVisible(false);

        Tab tab = operationsPane.getSelectionModel().getSelectedItem();

        if (tab.getText().equals(SIGNATURE) &&
                !textDataForSigning.getText().equals("")) {
            logEvent("Key changed. Resign data with with new key?");
            buttonReSign.setVisible(true);
            buttonVerifiedSignature.setVisible(false);
        } else if (tab.getText().equals(ENCRYPTION)) {
            logEvent("Key changed. Encrypt data with with new key?");
            buttonReEncrypt.setVisible(true);
            buttonVerifiedSignature.setVisible(false);
        } else if (tab.getText().equals(DECRYPTION)) {
            logEvent("Key changed. Decrypt data with with new key?");
            buttonReDecrypt.setVisible(true);
            buttonVerifiedSignature.setVisible(false);
        }
    }

    private void setSelectedKeyPair() {
        Label lKey = ownKeyList.getSelectionModel().getSelectedItem();
        if (lKey != null) {
            PrivateKey privateKey = keyController.getPrivateKey
                    (lKey.getText());
            PublicKey publicKey = keyController.getPublicKey(lKey.getText());

            textKeyType.setText(publicKey.getAlgorithm());
            textPrivateKey.setText(Base64.getEncoder().
                    encodeToString(privateKey.getEncoded()));
            textPublicKey.setText(Base64.getEncoder().
                    encodeToString(publicKey.getEncoded()));
        }
    }

    private void setSelectedPublicKey() {
        Label lKey = publicKeyList.getSelectionModel().getSelectedItem();
        PublicKey publicKey = keyController.getPublicKey(lKey.getText());
        clearKeysValues();
        textKeyType.setText(publicKey.getAlgorithm());
        textPublicKey.setText(Base64.getEncoder().
                encodeToString(publicKey.getEncoded()));
    }

    private void copyToClipboard(String dataToCopy) {
        final Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();
        content.putString(dataToCopy);
        clipboard.setContent(content);
    }

    @SuppressWarnings("unused")
    private void handleViewAbout(ActionEvent event) {
        viewAbout();
    }

    private void viewAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About chestnut ");
        alert.setHeaderText("About chestnut");
        alert.setResizable(true);
        alert.getDialogPane().setPrefSize(700, 500);

        String address = "https://gitlab.com/OsloMet-ABI/chestnut";
        TextArea details = new TextArea();
        details.setWrapText(false);
        details.setEditable(false);
        String text =
                "Chestnut is an open source PKI teaching tool." +
                        System.lineSeparator();
        text += "See [" +
                address + "] for more details." + System.lineSeparator();
        text += "Chestnut icon (CC BY-SA 3.0) from [https://en" +
                ".wikipedia.org/wiki/File:Horse-chestnut_fruit.svg]" +
                System.lineSeparator();
        text += System.lineSeparator();
        text += "The following shortcuts are available:" + System
                .lineSeparator();
        text += "[CTRL]+[G] : Generate keypair" + System.lineSeparator();
        text += "[CTRL]+[L] : Import keypair" + System.lineSeparator();
        text += "[CTRL]+[U] : Import public key" + System.lineSeparator();
        text += "[CTRL]+[B] : Open About window" + System.lineSeparator();
        text += "[CTRL]+[O] : Open log" + System.lineSeparator();
        text += "[CTRL]+[LEFT] : Select next tab left" + System.lineSeparator();
        text += "[CTRL]+[RIGHT] : Select next tab right " + System.lineSeparator();

        details.setText(text);

        alert.getDialogPane().setContent(details);
        alert.showAndWait();
    }

    @SuppressWarnings("unused")
    private void handleViewLog(ActionEvent event) {
        viewLog();
    }

    private void viewLog() {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("chestnut log");
        alert.setHeaderText("View event log");
        alert.setResizable(true);
        alert.getDialogPane().setPrefSize(480, 650);

        TextArea logTextArea = new TextArea();
        logTextArea.setMaxWidth(Double.MAX_VALUE);
        logTextArea.setMaxHeight(Double.MAX_VALUE);
        logTextArea.setText(logInfo.toString());
        logTextArea.setEditable(false);

        VBox log = new VBox();
        VBox.setVgrow(logTextArea, Priority.ALWAYS);
        log.getChildren().addAll(logTextArea);

        alert.getDialogPane().setContent(log);
        alert.showAndWait();
    }

    @SuppressWarnings("unused")
    private void handleGenerateKeyPair(ActionEvent event) {
        generateKeyPair();
    }

    private void generateKeyPair() {

        ObservableList<String> sizeOptions =
                FXCollections.observableArrayList("512", "1024", "2048");

        ObservableList<String> typeOptions =
                FXCollections.observableArrayList("RSA");

        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("chestnut");
        dialog.setHeaderText("Generate PKI keypair");

        ButtonType generateButtonType = new ButtonType("Generate", ButtonBar
                .ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(generateButtonType,
                ButtonType.CANCEL);


        GridPane gridpane = new GridPane();
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        gridpane.setPadding(new Insets(10, 10, 10, 10));

        ColumnConstraints dialogColumn1 = new ColumnConstraints();
        dialogColumn1.setPercentWidth(45);
        ColumnConstraints dialogColumn2 = new ColumnConstraints();
        dialogColumn2.setPercentWidth(45);
        dialogColumn2.setHgrow(Priority.ALWAYS);
        gridpane.getColumnConstraints().addAll(dialogColumn1, dialogColumn2);

        // Add the size
        final Label sizeLabel = new Label("Key size");
        final ComboBox<String> sizeComboBox = new ComboBox<>(sizeOptions);
        gridpane.add(sizeLabel, 0, 0);
        gridpane.add(sizeComboBox, 0, 1);
        sizeComboBox.setValue("2048");

        // Add the type
        final Label typeLabel = new Label("Algorithm type");
        final ComboBox<String> typeComboBox = new ComboBox<>(typeOptions);
        gridpane.add(typeLabel, 1, 0);
        gridpane.add(typeComboBox, 1, 1);
        typeComboBox.setValue("RSA");

        // Add local-name of key
        final Label nameLabel = new Label("Name");
        final TextField nameText = new TextField();
        gridpane.add(nameLabel, 0, 2);
        gridpane.add(nameText, 0, 3);
        GridPane.setColumnSpan(nameText, 2);
        Platform.runLater(() -> nameText.requestFocus());
        dialog.getDialogPane().setContent(gridpane);
        dialog.showAndWait();

        String name = nameText.getText();
        try {
            String key = keyController.createAndAddKey(name,
                    typeComboBox.getValue(),
                    Integer.valueOf(sizeComboBox.getValue()));

            if (key != null) {
                ownKeyList.getItems().add(new Label(key));
                int size = ownKeyList.getItems().size();
                ownKeyList.getSelectionModel().select(size - 1);
                setSelectedKeyPair();
                logEvent("Generated private/public key pair identified by ("
                        + key + ")");
                enableTabsThatRequireKeys();
            } else {
                logEvent("Could not generate private/public keypair. Does the " +
                        "name (" + name + ") you have chosen conflict with an" +
                        "existing public key or keypair?");
            }
        } catch (NoSuchAlgorithmException e) {
            logEvent(e.toString());
            logEvent("Could not generate private/public keypair");
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void handleXMLEncryptDocument(ActionEvent event) {
        try {
            String key = getSelectedKeyIdentifier();
            if (key != null) {

                File selectedFile = loadSaveDialog("Create encrypted file", "",
                        "XML Signature encryption file", "*.xml");

                if (selectedFile != null) {
                    String filename = selectedFile.getAbsolutePath();
                    FileOutputStream xmlFile = new FileOutputStream(filename);

                    String data = textDataForXMLEnc.getText();
                    StringBuilder encryptedKey = new StringBuilder();
                    StringBuilder encryptedData = new StringBuilder();
                    encryptionController.
                            encryptDataHybrid(encryptedKey, encryptedData,
                                    data, key, AES_ALGORITHM,
                                    ENCRYPT_RSA_DEFAULT);

                    encryptionController.
                            writeXMLDataToFile(
                                    encryptedData.toString(),
                                    encryptedKey.toString(), xmlFile);
                    xmlFile.flush();
                    xmlFile.close();
                    textDataXMLAsymmetricEnc.setText(encryptedData.toString());
                    textDataXMLSymmetricEnc.setText(encryptedKey.toString());
                    logEvent("Encrypted [" + filename + "] " + "using private" +
                            " key (" + key + ") to XML file using " +
                            "XMLSignature");
                }
            }
        } catch (IOException e) {
            logEvent(e.toString());
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void handleXMLDecryptDocument(ActionEvent event) {
        try {
            String key = getSelectedKeyIdentifier();
            if (key != null) {

                File selectedFile =
                        loadOpenDialog("Open encrypted file",
                                "XML Signature encryption file",
                                "*.xml");

                if (selectedFile != null) {

                    // Blank out the fields
                    textDataForXMLDec.setText("");
                    textDataXMLAsymmetricDec.setText("");
                    textDataXMLSymmetricDec.setText("");

                    String filename = selectedFile.getAbsolutePath();
                    FileInputStream xmlFile = new FileInputStream(filename);

                    StringBuilder encryptedSymmetricKey = new StringBuilder();
                    StringBuilder encryptedData = new StringBuilder();
                    String plainText = decryptionController.
                            readXMLDataFromFileAndDecrypt(encryptedData,
                                    encryptedSymmetricKey,
                                    filename, key, AES_ALGORITHM);
                    xmlFile.close();
                    textDataForXMLDec.setText(plainText);
                    textDataXMLAsymmetricDec.setText(encryptedSymmetricKey
                            .toString());
                    textDataXMLSymmetricDec.setText(encryptedData.toString());
                    logEvent("Decrypted [" + filename + "] " + "using private" +
                            " key (" + key + ") from XML file using " +
                            "XMLSignature");
                }

            }
        } catch (NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | IllegalBlockSizeException |
                InvalidAlgorithmParameterException | BadPaddingException |
                ParserConfigurationException | SAXException |
                IOException e) {

            logEvent(e.toString());
            if (e.getClass().isInstance(BadPaddingException.class)) {
                logEvent("Could not decrypt file using given key"
                );
            }
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void handleSignDocument(ActionEvent event) {
        try {
            buttonVerifiedDocumentSignature.setVisible(false);
            String key = getSelectedKeyIdentifier();
            if (key != null) {

                File selectedFile = loadSaveDialog("Create signature file", "",
                        "XML Signature signed file", "*.xml");

                if (selectedFile != null) {
                    String data = textToDocumentForSigning.getText();
                    String signature = signatureController.
                            signDataAndWriteToXMLFile(
                                    data, selectedFile.getAbsolutePath(), key,
                                    SHA_256_CHECKSUM, SIGN_SHA_256_RSA);

                    textToDocumentSignature.setText(signature);
                    logEvent("Signed [" + selectedFile.getAbsolutePath() + "]" +
                            " using private key (" + key + ")");
                }

            }
        } catch (NoSuchAlgorithmException | InvalidKeyException |
                SignatureException | IOException e) {
            logEvent(e.toString());
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void handleGenerateChecksumFile(ActionEvent event) {
        try {
            textDataForChecksum.setText("");
            File file = loadOpenDialog("Choose file to create checksum",
                    "Any file", "*.*");
            if (file == null) {
                return;
            }
            textFileLocationForChecksum.setText(file.getAbsolutePath());
            FileInputStream inputStream = new FileInputStream(file);
            DigestInputStream sha1Stream = new DigestInputStream(
                    inputStream, MessageDigest.getInstance(SHA_1));
            DigestInputStream md5Stream = new DigestInputStream(
                    sha1Stream, MessageDigest.getInstance(MD5));
            DigestInputStream sha256Stream = new DigestInputStream(
                    md5Stream, MessageDigest.getInstance(SHA_256));
            DigestInputStream sha512Stream = new DigestInputStream(
                    sha256Stream, MessageDigest.getInstance(SHA_512));
            byte[] buffer = new byte[1024];
            while (sha512Stream.read(buffer) > 0) {
            }

            // Get the MD5 checksum
            textMd5.setText(DatatypeConverter.printHexBinary(
                    md5Stream.getMessageDigest().digest()).toUpperCase());

            // Get the SHA-1 checksum
            textSha1.setText(DatatypeConverter.printHexBinary(
                    sha1Stream.getMessageDigest().digest()).toUpperCase());

            // Get the SHA-256 checksum
            textSha256.setText(DatatypeConverter.printHexBinary(
                    sha256Stream.getMessageDigest().digest()).toUpperCase());

            // Get the SHA-512 checksum
            textSha512.setText(DatatypeConverter.printHexBinary(
                    sha512Stream.getMessageDigest().digest()).toUpperCase());
            logEvent("Generated checksums for [" + file.getAbsolutePath() + "]");
        } catch (NoSuchAlgorithmException | IOException e) {
            logEvent(e.toString());
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void handleLoadDocument(ActionEvent event) {
        try {
            buttonVerifiedDocumentSignature.setVisible(false);
            buttonVerifiedFromDocumentSignature.setVisible(false);
            File signedXMLFile = loadOpenDialog("Open signature file",
                    "XML Signature signed file", "*.xml");
            StringBuilder data = new StringBuilder();
            StringBuilder signature = new StringBuilder();
            StringBuilder checksum = new StringBuilder();
            signatureController.readXMLDataFromFile(
                    new FileInputStream(signedXMLFile), data, signature,
                    checksum);
            textFromDocumentForSigning.setText(data.toString());
            textFromDocumentSignature.setText(signature.toString());
            logEvent("Loaded XML-document with signature from ["
                    + signedXMLFile.getAbsolutePath() + "]");

        } catch (IOException e) {
            logEvent("Could not load XML-document with signature");
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void handleVerifySigned(ActionEvent event) {
        String key = getSelectedKeyIdentifier();

        if (handleVerifyDocument(key)) {
            buttonVerifiedDocumentSignature.
                    setText("Signature is correct!");
            buttonVerifiedDocumentSignature.setVisible(true);
            buttonVerifiedDocumentSignature.setStyle(BUTTON_SUCCESS_SMALL);
            logEvent("Verified document data using key (" + key + ")");
        } else {
            buttonVerifiedDocumentSignature.setText("Signature is incorrect!");
            buttonVerifiedDocumentSignature.setVisible(true);
            buttonVerifiedDocumentSignature.setStyle(BUTTON_DANGER_SMALL);
            logEvent("Could not verify data using key (" + key + ")");
        }
    }

    @SuppressWarnings("unused")
    private void handleVerifySignedDocument(ActionEvent event) {
        String key = getSelectedKeyIdentifier();

        if (handleVerifyDocument(key)) {
            buttonVerifiedFromDocumentSignature.
                    setText("Signature is correct!");
            buttonVerifiedFromDocumentSignature.setVisible(true);
            buttonVerifiedFromDocumentSignature.setStyle(BUTTON_SUCCESS_SMALL);
            logEvent("Verified document data using key (" + key + ")");
        } else {
            buttonVerifiedFromDocumentSignature.setText("Signature is incorrect!");
            buttonVerifiedFromDocumentSignature.setVisible(true);
            buttonVerifiedFromDocumentSignature.setStyle(BUTTON_DANGER_SMALL);
            logEvent("Could not verify data using key (" + key + ")");
        }
    }

    private boolean handleVerifyDocument(String key) {

        if (!textToDocumentSignature.getText().equals("")) {
            try {
                String data = textToDocumentForSigning.getText();
                String signature = textToDocumentSignature.getText();
                return signatureController.
                        verifySignatureEncodedBase64(data, signature, key,
                                SIGN_SHA_256_RSA);
            } catch (NoSuchAlgorithmException | InvalidKeyException |
                    SignatureException e) {
                logEvent(e.toString());
                e.printStackTrace();
            }
        }
        return false;
    }

    private void logEvent(String event) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String date = sdf.format(new Date());
        logInfo.append(date);
        logInfo.append(":");
        logInfo.append(event);
        logInfo.append(System.lineSeparator());
        statusBar.setText(event);
    }

    @Override
    public void init() {
        keyController = new KeyController();
        signatureController = new SignatureController(keyController);
        encryptionController = new EncryptionController(keyController);
        decryptionController = new DecryptionController(keyController);
    }

    private void selectLeftTab() {

        if (operationsPane.getSelectionModel().isSelected(0)) {
            operationsPane.getSelectionModel().selectLast();
        } else {
            operationsPane.getSelectionModel().selectPrevious();
        }
    }

    private void selectRightTab() {
        int size = operationsPane.getTabs().size();
        if (operationsPane.getSelectionModel().isSelected(size - 1)) {
            operationsPane.getSelectionModel().selectFirst();
        } else {
            operationsPane.getSelectionModel().selectNext();
        }
    }

    private void enableTabsThatRequireKeys() {
        signingTab.setDisable(false);
        encryptionTab.setDisable(false);
        decryptionTab.setDisable(false);
        xmlSigningTab.setDisable(false);
        xmlEncryptionTab.setDisable(false);
        xmlDecryptionTab.setDisable(false);
    }

    private void setRowConstraints(GridPane gridPane) {
        for (int i = 0; i < 22; i++) {
            RowConstraints rowConstraint = new RowConstraints();
            rowConstraint.setMinHeight(20);
            rowConstraint.setMaxHeight(20);
            rowConstraint.setPrefHeight(20);
            gridPane.getRowConstraints().add(rowConstraint);
        }
    }

    /**
     * This is meant for use at OsloMet where students will use this from
     * windows. Students save to System.getenv("APPDATA") rather
     * System.getProperty("user.home"). So we first try to see if we can
     * retrieve APPDATA, otherwise use user.home.
     *
     * @return the home directory
     */
    private String getHome() {
        String home = System.getenv("APPDATA");
        if (home == null) {
            home = System.getProperty("user.home");
        }
        return home;
    }
}
