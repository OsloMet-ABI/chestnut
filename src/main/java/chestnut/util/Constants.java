package chestnut.util;

public final class Constants {

    public static final String AES_ALGORITHM = "AES/CBC/PKCS5PADDING";
    public static final String SIGN_SHA_256_RSA = "SHA256withRSA";
    public static final String ENCRYPT_RSA_DEFAULT = "RSA/ECB/PKCS1Padding";
    public static final String ENCRYPT_AES_DEFAULT = "AES/ECB/PKCS5Padding";
    public static final String RSA = "RSA";
    public static final String SYMMETRIC_ALGORITHM = "AES";
    public static final String SHA_256_CHECKSUM = "SHA-256";
    public static final String SHA_256 = "SHA-256";
    public static final String SHA_512 = "SHA-512";
    public static final String SHA_1 = "SHA1";
    public static final String MD5 = "MD5";
    public static final String PRIVATE = "PRIVATE";
    public static final String PRIVATE_PUBLIC_KEYPAIR =
            "PRIVATE-PUBLIC-KEYPAIR";
    public static final String PUBLIC = "PUBLIC";

    public static final int DEFAULT_KEY_SIZE = 2048;


    // GUI Stuff
    public static final String BUTTON = "-fx-background-color:  #286090; " +
            "-fx-text-fill: white; fx-font-weight: bold;";
    public static final String BACKGROUND = "-fx-background-color:  #f4f4f4;";
    public static final String BUTTON_DANGER = "-fx-background-color: #dc3545;"
            + " -fx-text-fill: white; -fx-font-size: 6pt;";
    public static final String BUTTON_DANGER_SMALL = "-fx-background-color: " +
            "#dc3545;"
            + " -fx-text-fill: white;";
    public static final String BUTTON_SUCCESS = " -fx-background-color: #28a745;"
            + " -fx-text-fill: white;";
    public static final String BUTTON_SUCCESS_SMALL = " -fx-background-color:" +
            " #28a745; -fx-text-fill: white; -fx-font-size: 6pt;";

    public static final String SIGNATURE = "Signing";
    public static final String ENCRYPTION = "Encryption";
    public static final String DECRYPTION = "Decryption";

    public static final String TT_KEY_PANEL =
            "To sign/encrypt/decrypt using asymmetric keys you need to first " +
                    "generate or load keys that the application can use";
    public static final String TT_GENERATE_KEY =
            "Generate a private/public key pair";
    public static final String TT_LOAD_KEY =
            "Import a private/public keypair from .key and .pub files";
    public static final String TT_LOAD_PUBLIC =
            "Import a public key from a .pub file";
    public static final String TT_KEYS_TAB =
            "See the selected key (pair)";
    public static final String TT_SIGNING_TAB =
            "Sign some data using a private key";
    public static final String TT_ENCRYPTION_TAB =
            "Encrypt data using a public key";
    public static final String TT_DECRYPTION_TAB =
            "Decrypt data using a private key";
    public static final String TT_XML_SIGNING_TAB =
            "Sign data using XMLSignature library and export/import as XML.";
    public static final String TT_XML_ENC_TAB =
            "Encrypt data using XMLSignature library and export as XML.";
    public static final String TT_XML_DEC_TAB =
            "Decrypt data using XMLSignature library and import from XML.";
    public static final String TT_SYMMETRIC_DEC_TAB =
            "Encrypt data using a symmetric encryption algorithm";
    public static final String TT_SYMMETRIC_ENC_TAB =
            "Decrypt data using a symmetric encryption algorithm";
    public static final String TT_CHECKSUM_TAB =
            "Generate checksums for data or files";
    public static final String TT_BASE64_TAB =
            "Generate base64 from plain text";
    public static final String TT_UUID_TAB =
            "Generate a number of UUIDs";
}
