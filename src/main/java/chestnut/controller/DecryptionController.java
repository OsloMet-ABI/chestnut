package chestnut.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.security.*;
import java.util.Base64;

import static chestnut.util.Constants.ENCRYPT_RSA_DEFAULT;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * There are a lot of exceptions that can occur. Exception handling is the
 * responsibility of the caller.
 * <p>
 * This class uses the following XML as a template of how encrypted
 * information is marked up:
 *
 * <EncryptedData xmlns="http://www.w3.org/2001/04/xmlenc#" Type="http://www.w3.org/2001/04/xmlenc#Element">
 * <EncryptionMethod Algorithm="http://www.w3.org/2001/04/xmlenc#tripledes-cbc"/>
 * <KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
 * <EncryptedKey xmlns="http://www.w3.org/2001/04/xmlenc#">
 * <EncryptionMethod Algorithm="http://www.w3.org/2001/04/xmlenc#rsa-1_5"/>
 * <KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
 * <KeyName/>
 * </KeyInfo>
 * <CipherData>
 * <CipherValue/>
 * </CipherData>
 * </EncryptedKey>
 * </KeyInfo>
 * <CipherData>
 * <CipherValue/>
 * </CipherData>
 * </EncryptedData>
 */

public class DecryptionController {

    private static final Logger logger =
            LoggerFactory.getLogger(DecryptionController.class);

    private KeyController keyController;

    public DecryptionController(KeyController keyController) {
        this.keyController = keyController;
    }

    /**
     * read an XML document that contains <EncryptedData> and return the
     * decrypted data
     * <p>
     * symmetricAlgorithm. Note this could be retrieved from
     * http://www.w3.org/2001/04/xmlenc#tripledes-cbc
     *
     * @param filename           The name including directory of the XML file
     * @param privateKeyKey      The private key identifier to decode the data with
     * @param symmetricAlgorithm The type of symmetric algorithm
     * @return The encrypted data
     */
    public String readXMLDataFromFileAndDecrypt(
            StringBuilder encryptedSymmetricKey,
            StringBuilder encryptedData,
            String filename, String privateKeyKey,
            String symmetricAlgorithm)
            throws NoSuchAlgorithmException, InvalidKeyException,
            NoSuchPaddingException, IllegalBlockSizeException,
            InvalidAlgorithmParameterException, BadPaddingException,
            ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        File xmlFile = new File(filename);
        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);
        encryptedSymmetricKey.append(getEncryptedData(document, 0));
        encryptedData.append(getEncryptedData(document, 1));
        return decryptDataHybrid(encryptedData.toString(),
                encryptedSymmetricKey.toString(),
                privateKeyKey, symmetricAlgorithm);
    }

    public String decryptDataHybrid(String encryptedData,
                                    String encryptedKey,
                                    String privateKeyKey,
                                    String symmetricAlgorithm)
            throws NoSuchAlgorithmException, InvalidKeyException,
            NoSuchPaddingException, IllegalBlockSizeException,
            InvalidAlgorithmParameterException, BadPaddingException {
        PrivateKey privateKey = keyController.getPrivateKey(privateKeyKey);
        SecretKey symmetricKey = decryptSymmetricKey(encryptedKey,
                privateKey, symmetricAlgorithm);
        return decryptDataWithSymmetricKey(encryptedData, symmetricKey,
                symmetricAlgorithm);

    }


    /**
     * There are two CipherValues in the document. The first one is the one
     * we are out after here.
     * <p>
     * This could be made more robust picking up the exact structure from the
     * root, but as a teaching tool we can tolerate a little slack. Can be fixed
     * later.
     *
     * @param keyOrData 0 is for the symmetric key, 1 is for data
     * @param document  The XML-document
     * @return the encrypted symmetric key
     */
    private String getEncryptedData(Document document, int keyOrData) {

        String signatureValue = null;

        NodeList nodeList = document.getElementsByTagName("CipherValue");
        if (nodeList.getLength() < 1) {
            logger.info("Attempt to open file without a CipherValue " +
                    "element");
            return null;
        } else if (nodeList.getLength() > 2) {
            logger.info("Attempt to open file with too many CipherValue " +
                    "elements");
            return null;
        }

        Node node = nodeList.item(keyOrData);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            signatureValue = element.getTextContent();
            logger.info("Found CipherValue value: [" + signatureValue + "]");
        }
        return signatureValue;
    }

    /**
     * Decrypt a symmetrically encrypted key stored in the variable encryptedKey
     * using the privateKey provided. To regenerate the symmetric key as a
     * SecretKey object, we need the symmetricKey algorithm
     *
     * @param encryptedKey       The data (symmetric key e.g DES/AES) encrypted using
     *                           an asymmetric key (e.g RSA)
     * @param privateKey         The private key to decrypt the data with
     * @param symmetricAlgorithm The symmetric key type (e.g DES/AES)
     * @return the symmetric key as a SecretKey object
     * @throws NoSuchAlgorithmException  because the algorithm is not supported
     * @throws NoSuchPaddingException    problem with padding
     * @throws InvalidKeyException       problem with the key
     * @throws IllegalBlockSizeException block size is wrong
     * @throws BadPaddingException       problem with padding
     */
    private SecretKey decryptSymmetricKey(String encryptedKey,
                                          PrivateKey privateKey,
                                          String symmetricAlgorithm)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException {

        Cipher cipher = Cipher.getInstance(ENCRYPT_RSA_DEFAULT);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decodedKey = cipher.doFinal(
                Base64.getDecoder().decode(encryptedKey));
        return new SecretKeySpec(decodedKey, 0, decodedKey.length,
                symmetricAlgorithm.substring(0,
                        symmetricAlgorithm.indexOf('/')));
    }

    /**
     * Decrypt the encrypted data stored in the variable data. The key used to
     * decrypt the data must also be provided
     *
     * @param data The encrypted data, encoded using base64
     * @param key  The key used to do the decryption
     * @return The decrypted data as a String
     * @throws NoSuchAlgorithmException  because the algorithm is not supported
     * @throws NoSuchPaddingException    problem with padding
     * @throws InvalidKeyException       problem with the key
     * @throws IllegalBlockSizeException block size is wrong
     * @throws BadPaddingException       problem with padding
     */
    private String decryptDataWithSymmetricKey
    (String data, Key key, String algorithm)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException, InvalidAlgorithmParameterException {
        byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, key, ivspec);
        byte[] decryptedData = cipher.doFinal(Base64.getDecoder().decode(data));
        return new String(decryptedData, UTF_8);
    }

    /**
     * Decrypt the encrypted data stored in the variable data. The key used to
     * decrypt the data must also be provided
     *
     * @param data The encrypted data, encoded using base64
     * @param key  The key used to do the decryption
     * @return The decrypted data as a String
     * @throws NoSuchAlgorithmException  because the algorithm is not supported
     * @throws NoSuchPaddingException    problem with padding
     * @throws InvalidKeyException       problem with the key
     * @throws IllegalBlockSizeException block size is wrong
     * @throws BadPaddingException       problem with padding
     */
    private String decryptDataWithAsymmetricKey(
            String data, Key key, String algorithm)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException {
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decryptedData = cipher.doFinal(Base64.getDecoder().decode(data));
        return new String(decryptedData, UTF_8);
    }

    /**
     * Decrypt the encrypted data stored in the variable data. The key used to
     * decrypt the data must also be provided
     *
     * @param data          The encrypted data
     * @param privateKeyKey Identifier of the private key used to do the
     *                      decryption
     * @return The decrypted data as a String
     * @throws NoSuchAlgorithmException  because the algorithm is not supported
     * @throws NoSuchPaddingException    problem with padding
     * @throws InvalidKeyException       problem with the key
     * @throws IllegalBlockSizeException block size is wrong
     * @throws BadPaddingException       problem with padding
     */
    public String decryptData(String data,
                              String privateKeyKey,
                              String algorithm)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException {
        PrivateKey privateKey = keyController.getPrivateKey(privateKeyKey);
        return decryptDataWithAsymmetricKey(data, privateKey, algorithm);
    }
}
