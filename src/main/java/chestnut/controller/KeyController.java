package chestnut.controller;

import chestnut.keygen.ChestnutKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import static chestnut.util.Constants.*;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * KeyContoller: Handle the creation, saving and loading of keys
 */
public class KeyController {

    private static final Logger logger =
            LoggerFactory.getLogger(KeyController.class);

    private Base64.Encoder encoder = Base64.getEncoder();

    private HashMap<String, ChestnutKeyGenerator> keyGenerators = new HashMap<>();
    private HashMap<String, PublicKey> publicKeys = new HashMap<>();

    public KeyController() {
    }


    /**
     * retrieve a public key. Public keys can be stored either in publicKeys or
     * keyGenerators object. If the given public key identifier is not found a
     * null is returned.
     *
     * @param publicKeyKey key identifier used to hold the key (map key->value)
     * @return the public key or null if we don't have one
     */
    public PublicKey getPublicKey(String publicKeyKey) {

        PublicKey publicKey = publicKeys.get(publicKeyKey);

        if (publicKey == null) {
            ChestnutKeyGenerator chestnutKeyGenerator = keyGenerators.get(publicKeyKey);
            if (chestnutKeyGenerator != null) {
                publicKey = chestnutKeyGenerator.getKeyPair().getPublic();
            }
        }

        return publicKey;
    }

    /**
     * Retrieve a private key from the lists of keys stored.
     *
     * @param privateKeyKey key identifier used to hold the key (map key->value)
     * @return the private key or null if we don't have one
     */
    public PrivateKey getPrivateKey(String privateKeyKey) {

        ChestnutKeyGenerator chestnutKeyGenerator = keyGenerators.get(privateKeyKey);
        if (chestnutKeyGenerator != null) {
            return chestnutKeyGenerator.getKeyPair().getPrivate();
        }
        return null;
    }

    /**
     * Add a key combination
     * <p>
     * keyGenerators is a map of key -> KeyPairs that contains private and
     * public keys. Here we can add an instance creating a mapping like
     * "alice" -> (private-key-alice, pub-key-alice)
     * <p>
     * It's a bit unfortunate that we use the map terminology for "index" as
     * "key" and key as in private or public key.
     *
     * @param key                  The key identifier (for the map)
     * @param chestnutKeyGenerator A set of private, public key pain
     */
    private void addKeyGenerator(String key,
                                 ChestnutKeyGenerator chestnutKeyGenerator) {
        if (!keyAlreadyUsed(key)) {
            keyGenerators.put(key, chestnutKeyGenerator);
        } else {
            logger.info("Cannot add key, key identifier already used!");
        }
    }

    /**
     * Fetch all the keys
     * <p>
     * keyGenerators is a map of key -> KeyPairs that contains private and
     * public keys. Here we can add an instance creating a mapping like
     * "alice" -> (private-key-alice, pub-key-alice)
     * <p>
     * It's a bit unfortunate that we use the map terminology for "index" as
     * "key" and key as in private or public key.
     * <p>
     * This method should not really be required, but we are using it in the
     * testing part of the application
     */
    public HashMap<String, ChestnutKeyGenerator> getKeys() {
        return keyGenerators;
    }

    /**
     * Note: Currently the application just works with RSA for simplicity
     * sake. Later we may change this but it will require knowing
     * the format of keys when loading them.
     *
     * @param algorithm The algorithm to use when creating a key
     * @param keySize   The size of the key you wish to generate
     * @throws NoSuchAlgorithmException if the algorithm specified is not
     *                                  supported.
     */
    public String createAndAddKey(String key, String algorithm, int keySize)
            throws NoSuchAlgorithmException {
        ChestnutKeyGenerator chestnutKeyGenerator = new ChestnutKeyGenerator();
        chestnutKeyGenerator.generateKeyPair(algorithm, keySize);

        if (!keyAlreadyUsed(key)) {
            keyGenerators.put(key, chestnutKeyGenerator);
            logger.info("Created a key pair set");
            return key;
        } else {
            logger.info("Cannot add key, key identifier already used!");
            return null;
        }
    }

    /**
     * Convenience method to write both public and private keys to disk
     *
     * @param directory          The directory to store the files in
     * @param publicKeyFilename  the name of the public key file without
     *                           extension
     * @param privateKeyFilename the name of the private key file without
     *                           extension
     * @param key                internal identifier for the keys
     * @throws IOException if there is a problem writing the data to disk
     */
    public void saveKeysToDisk(String key, String directory,
                               String publicKeyFilename,
                               String privateKeyFilename)
            throws IOException {

        ChestnutKeyGenerator chestnutKeyGenerator = keyGenerators.get(key);

        if (chestnutKeyGenerator != null) {
            KeyPair keyPair = chestnutKeyGenerator.getKeyPair();
            PrivateKey privateKey = keyPair.getPrivate();
            String filename = directory + File.separator + privateKeyFilename +
                    ".key";
            FileWriter outPrivate = new FileWriter(filename);
            logger.info("Saved " + privateKey.getAlgorithm() + " private key to [" +
                    filename + "]");

            savePrivateKeyToDisk(outPrivate, privateKey);

            filename = directory + File.separator + publicKeyFilename + ".pub";
            FileWriter outPublic = new FileWriter(filename);
            PublicKey publicKey = keyPair.getPublic();
            savePublicKeyToDisk(outPublic, publicKey);
            logger.info("Saved " + publicKey.getAlgorithm() + " public key to [" +
                    filename + "]");
        }
    }

    /**
     * Saves the private key to disk base64 encoded.
     * <p>
     * Note: we are adding the file extension as students might easily forget
     * to put correct file extension.
     *
     * @param out        Handle to file
     * @param privateKey actual private key
     * @throws IOException if there is a problem writing the key to disk
     */
    public void savePrivateKeyToDisk(FileWriter out,
                                     PrivateKey privateKey) throws IOException {
        out.write("-----BEGIN RSA PRIVATE KEY-----\n");
        out.write(encoder.encodeToString(privateKey.getEncoded()));
        out.write("\n-----END RSA PRIVATE KEY-----\n");
        out.close();
    }

    /**
     * Saves the private key to disk base64 encoded.
     * <p>
     * Note: we are adding the file extension as students might easily forget
     * to put correct file extension.
     *
     * @param out       Handle to the file
     * @param publicKey The actual public key
     * @throws IOException if there is a problem writing the key to disk
     */
    public void savePublicKeyToDisk(FileWriter out,
                                    PublicKey publicKey)
            throws IOException {

        out.write("-----BEGIN RSA PUBLIC KEY-----\n");
        out.write(encoder.encodeToString(publicKey.getEncoded()));
        out.write("\n-----END RSA PUBLIC KEY-----\n");
        out.close();
    }

    /**
     * Load a private key from disk
     *
     * @param privateKeyFile the file containing the private key
     * @param algorithm      The algorithm the key was generated with
     * @return The private key
     * @throws IOException              Problem reading the file
     * @throws NoSuchAlgorithmException Algorithm not supported
     * @throws InvalidKeySpecException  Problem with the key
     */
    private PrivateKey loadPrivateKeyFromDisk(Path privateKeyFile,
                                              String algorithm)
            throws IOException, NoSuchAlgorithmException,
            InvalidKeySpecException {
        List<String> lines = Files.readAllLines(privateKeyFile, UTF_8);
        if (lines.size() != 3)
            throw new IllegalArgumentException("Not a value base64 encoded " +
                    "file with 3 lines");
        if (!lines.remove(0).startsWith("--"))
            throw new IllegalArgumentException("Expected header is missing ");
        if (!lines.remove(lines.size() - 1).startsWith("--"))
            throw new IllegalArgumentException("Expected footer is missing");

        byte[] bytes = Base64.getDecoder().decode(String.join("", lines));

        PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        return keyFactory.generatePrivate(ks);
    }

    /**
     * @param publicKeyFile the file containing the public key
     * @param algorithm     The algorithm the key was generated with
     * @return The public key
     * @throws IOException              Problem reading the file
     * @throws NoSuchAlgorithmException Algorithm not supported
     * @throws InvalidKeySpecException  Problem with the key
     */
    private PublicKey loadPublicKeyFromDisk(Path publicKeyFile,
                                            String algorithm)
            throws IOException, NoSuchAlgorithmException,
            InvalidKeySpecException {

        List<String> lines = Files.readAllLines(publicKeyFile, UTF_8);
        if (lines.size() != 3)
            throw new IllegalArgumentException("Not a value base64 encoded " +
                    "file with 3 lines");
        if (!lines.remove(0).startsWith("--"))
            throw new IllegalArgumentException("Expected header is missing ");
        if (!lines.remove(lines.size() - 1).startsWith("--"))
            throw new IllegalArgumentException("Expected footer is missing");

        byte[] bytes = Base64.getDecoder().decode(String.join("", lines));
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        return keyFactory.generatePublic(keySpec);
    }

    /**
     * Convenience method to read both public and private keys from disk
     *
     * @param privateKeyFile name of the private key file
     * @param publicKeyFile  name of the public key file
     * @throws IOException              problem reading data from storage
     * @throws NoSuchAlgorithmException algorithm is not supported
     * @throws InvalidKeySpecException  problem with the keys
     */
    public void loadKeysFromDisk(String key, Path privateKeyFile, Path
            publicKeyFile, String algorithm)
            throws IOException, NoSuchAlgorithmException,
            InvalidKeySpecException {

        PrivateKey privateKey = loadPrivateKeyFromDisk(privateKeyFile, algorithm);
        PublicKey publicKey = loadPublicKeyFromDisk(publicKeyFile, algorithm);

        ChestnutKeyGenerator chestnutKeyGenerator = new ChestnutKeyGenerator(
                new KeyPair(publicKey, privateKey));

        if (!keyAlreadyUsed(key)) {
            addKeyGenerator(key, chestnutKeyGenerator);
        }
    }

    /**
     * Load just a public key from disk
     *
     * @param publicKeyFile name of the public key file
     * @throws IOException              problem reading data from storage
     * @throws NoSuchAlgorithmException algorithm is not supported
     * @throws InvalidKeySpecException  problem with the keys
     */
    public boolean loadPublicKeyFromDisk(String key, Path publicKeyFile,
                                         String algorithm)
            throws IOException, NoSuchAlgorithmException,
            InvalidKeySpecException {

        PublicKey publicKey = loadPublicKeyFromDisk(publicKeyFile, algorithm);
        if (!keyAlreadyUsed(key)) {
            publicKeys.put(key, publicKey);
        } else {
            logger.info("Cannot add key, key identifier already used!");
            return false;
        }
        return true;
    }

    /**
     * Load a public and private key
     *
     * @param privateKey The privateKey
     * @param publicKey The publicKey
     * @param algorithm The algorithm the keys adhere to
     * @param key The how to identify the key pain
     * @throws NoSuchAlgorithmException algorithm is not supported
     * @throws InvalidKeySpecException  problem with the keys
     */
    public boolean addKeys(String privateKey, String publicKey, String
            algorithm, String key)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        if (!keyAlreadyUsed(key)) {
            keyGenerators.put(key,
                    new ChestnutKeyGenerator(privateKey, publicKey, algorithm));
        } else {
            logger.info("Cannot add key, key identifier already used!");
            return false;
        }
        return true;
    }

    /**
     * Remove all the keys we currently have control over
     */
    public void clearKeys() {
        publicKeys.clear();
        keyGenerators.clear();
    }

    private boolean keyAlreadyUsed(String key) {
        return publicKeys.get(key) != null ||
                keyGenerators.get(key) != null;
    }


    public String getKeyAndKeyType(Path keyFile, StringBuilder key) {

        try {
            List<String> lines = Files.readAllLines(keyFile, UTF_8);

            if (lines.size() != 3) {
                logger.info(keyFile.getFileName() + " does not " +
                        "contain exactly three lines");
                return null;
            }
            if (!lines.get(0).startsWith("--")) {
                logger.info(keyFile.getFileName() + " does not " +
                        "start with --");
                return null;
            }
            if (lines.get(0).contains("BEGIN RSA PRIVATE KEY")) {
                key.append(lines.get(1));
                return PRIVATE;
            }
            if (lines.get(0).contains("BEGIN RSA PUBLIC KEY")) {
                key.append(lines.get(1));
                return PUBLIC;
            }
        } catch (IOException e) {
            logger.error("Error reading " + keyFile.getFileName());
            return null;
        }
        logger.info(keyFile.getFileName() + " does not " +
                " either 'BEGIN RSA PRIVATE KEY' or 'BEGIN RSA " +
                "PUBLIC KEY'");
        return null;
    }

    public String getKeyType(String key) {
        if (keyGenerators.get(key) != null) {
            return PRIVATE_PUBLIC_KEYPAIR;
        }
        if (publicKeys.get(key) != null) {
            return PUBLIC;
        }
        return "UNKNOWN";
    }
}
