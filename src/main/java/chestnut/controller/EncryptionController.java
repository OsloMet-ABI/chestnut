package chestnut.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.*;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * This class uses the following XML as a template to produce encrypted
 * information:
 *
 * <EncryptedData xmlns="http://www.w3.org/2001/04/xmlenc#" Type="http://www.w3.org/2001/04/xmlenc#Element">
 * <EncryptionMethod Algorithm="http://www.w3.org/2001/04/xmlenc#tripledes-cbc"/>
 * <KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
 * <EncryptedKey xmlns="http://www.w3.org/2001/04/xmlenc#">
 * <EncryptionMethod Algorithm="http://www.w3.org/2001/04/xmlenc#rsa-1_5"/>
 * <KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
 * <KeyName/>
 * </KeyInfo>
 * <CipherData>
 * <CipherValue/>
 * </CipherData>
 * </EncryptedKey>
 * </KeyInfo>
 * <CipherData>
 * <CipherValue/>
 * </CipherData>
 * </EncryptedData>
 */

public class EncryptionController {

    private static final Logger logger =
            LoggerFactory.getLogger(EncryptionController.class);

    private KeyController keyController;

    public EncryptionController(KeyController keyController) {
        this.keyController = keyController;
    }

    /**
     * Encrypt data in variable data and write the encrypted data to the
     * XML-file identified by filename. Encrypt using the data using the
     * public key identifier (need to do a look-up). Both symmetric and
     * asymmetric keys
     * are required.
     * <p>
     * The symmetric key algorithm is used to encrypt the data and the
     * symmetric key is encrypted using the public key.
     *
     * @param data               The data to encrypt
     * @param filename           the name of the file to put the encrypted data into
     * @param publicKeyKey       the public key identifier
     * @param symmetricAlgorithm which algorithm to use when encrypting the data
     */
    public void encryptDataAndWriteToXMLFile(String data, String filename,
                                             String publicKeyKey,
                                             String symmetricAlgorithm,
                                             String asymmetricAlgorithm) {
        try {
            FileOutputStream xmlFile = new FileOutputStream(filename);
            StringBuilder encryptedKey = new StringBuilder();
            StringBuilder encryptedData = new StringBuilder();
            encryptDataHybrid(encryptedKey, encryptedData, data, publicKeyKey,
                    symmetricAlgorithm, asymmetricAlgorithm);
            writeXMLDataToFile(encryptedData.toString(),
                    encryptedKey.toString(), xmlFile);
        } catch (FileNotFoundException e) {
            logger.error(e.toString());
        }
    }

    /**
     * Encrypt data in variable data and write the encrypted data to the
     * XML-file identified by filename. Encrypt using the data using the
     * public key identifier (need to do a look-up). Both symmetric and
     * asymmetric keys
     * are required.
     * <p>
     * The symmetric key algorithm is used to encrypt the data and the
     * symmetric key is encrypted using the public key.
     *
     * @param data               The data to encrypt
     * @param publicKeyKey       the public key identifier
     * @param symmetricAlgorithm which algorithm to use when encrypting the data
     */
    public void encryptDataHybrid(StringBuilder encryptedKey,
                                  StringBuilder encryptedData,
                                  String data,
                                  String publicKeyKey,
                                  String symmetricAlgorithm,
                                  String asymmetricAlgorithm) {
        try {
            PublicKey publicKey = keyController.getPublicKey(publicKeyKey);
            SecretKey symmetricKey = generateSymmetricalKey(symmetricAlgorithm);
            encryptedKey.append(encryptKey(symmetricKey, publicKey,
                    asymmetricAlgorithm));
            encryptedData.append(encryptDataWithSymmetricKey(symmetricKey, data,
                    symmetricAlgorithm));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | IllegalBlockSizeException |
                InvalidAlgorithmParameterException |
                BadPaddingException e) {
            logger.error(e.toString());
        }
    }

    /**
     * Generate a random symmetric key in the SecretKey format
     * <p>
     * Look until you find a /
     *
     * @param symmetricAlgorithm The algorithm to use
     * @return The key as SecretKey
     * @throws NoSuchAlgorithmException if the algorithm is not supported
     */
    private SecretKey generateSymmetricalKey(String symmetricAlgorithm) throws
            NoSuchAlgorithmException {
        KeyGenerator keyGenerator =
                KeyGenerator.getInstance(
                        symmetricAlgorithm.substring(0,
                                symmetricAlgorithm.indexOf('/')));
        keyGenerator.init(128);
        return keyGenerator.generateKey();
    }

    /**
     * Encrypt data using a given key and chosen algorithm. Note this can
     * be used to encrypt with both symmetrical and asymmetrical keys.
     *
     * @param symmetricKey The symmetric to encrypt with
     * @param data         The data
     * @param algorithm    The algorithm to use
     * @return the encrypted data base64 encoded
     * @throws NoSuchAlgorithmException  Algorithm not supported
     * @throws NoSuchPaddingException    Padding not supported
     * @throws InvalidKeyException       Problem with the key
     * @throws IllegalBlockSizeException Problem with block size
     * @throws BadPaddingException       Problem with padding
     */
    private String encryptDataWithSymmetricKey(Key symmetricKey, String data,
                                               String algorithm) throws
            NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException, InvalidAlgorithmParameterException {

        byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, symmetricKey, ivspec);
        return Base64.getEncoder().
                encodeToString(cipher.doFinal(data.getBytes(UTF_8)));
    }

    /**
     * Encrypt data using a given key and chosen algorithm. Note this can
     * be used to encrypt with both symmetrical and asymmetrical keys.
     *
     * @param symmetricKey The symmetric to encrypt with
     * @param data         The data
     * @param algorithm    The algorithm to use
     * @return the encrypted data base64 encoded
     * @throws NoSuchAlgorithmException  Algorithm not supported
     * @throws NoSuchPaddingException    Padding not supported
     * @throws InvalidKeyException       Problem with the key
     * @throws IllegalBlockSizeException Problem with block size
     * @throws BadPaddingException       Problem with padding
     */
    private String encryptDataWithAsymmetricKey(Key symmetricKey, String data,
                                                String algorithm) throws
            NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException {


        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, symmetricKey);
        return Base64.getEncoder().
                encodeToString(cipher.doFinal(data.getBytes(UTF_8)));
    }


    public String encryptDataWithPublicKey(
            String publicKeyKey, String data, String algorithm) throws
            NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException {
        PublicKey publicKey = keyController.getPublicKey(publicKeyKey);
        return encryptDataWithAsymmetricKey(publicKey, data, algorithm);
    }

    /**
     * Encrypt a symmetric key using the given public key and return the
     * encrypted key base64 encoded.
     *
     * @param symmetricKey The key data you wish to encrypt
     * @param publicKey    The public key to use
     * @return The encrypted symmetric key
     * @throws NoSuchAlgorithmException  Algorithm not supported
     * @throws NoSuchPaddingException    Padding not supported
     * @throws InvalidKeyException       Problem with the key
     * @throws IllegalBlockSizeException Problem with block size
     * @throws BadPaddingException       Problem with padding
     */
    private String encryptKey(SecretKey symmetricKey, PublicKey publicKey,
                              String algorithm)
            throws
            NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException {

        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return Base64.getEncoder().
                encodeToString(cipher.doFinal(symmetricKey.getEncoded()));
    }

    /**
     * Write encrypted data to XML document as per XML-structure shown at top.
     * <p>
     * Note as is usual the public key is used to encrypt a symmetric key that
     * the data is encrypted according to.
     *
     * @param encryptedDataData The encrypted data (encrypted symmetrically)
     * @param encryptedKeyData  The symmetric key encrypted using the public key
     * @param xmlFile           The file to write to
     */
    public void writeXMLDataToFile(String encryptedDataData,
                                   String encryptedKeyData,
                                   OutputStream xmlFile) {

        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;

        try {

            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            // Create:  EncryptedData xmlns="http://www.w3.org/2001/04/xmlenc#"
            //          Type="http://www.w3.org/2001/04/xmlenc#Element">
            Element documentElement = document.createElementNS(
                    "http://www.w3.org/2001/04/xmlenc", "EncryptedData");
            documentElement.setAttribute("Type",
                    "http://www.w3.org/2001/04/xmlenc#Element");
            document.appendChild(documentElement);

            // Create:  <EncryptionMethod Algorithm=
            //          "http://www.w3.org/2001/04/xmlenc#tripledes-cbc"/>
            Element encryptionMethod = document.createElement("EncryptionMethod");
            encryptionMethod.setAttribute("Algorithm",
                    "http://www.w3.org/2001/04/xmlenc#tripledes-cbc");

            documentElement.appendChild(encryptionMethod);

            // Create: <KeyInfo>
            Element keyInfo = document.createElementNS(
                    "http://www.w3.org/2000/09/xmldsig#", "KeyInfo");
            documentElement.appendChild(keyInfo);

            // Create: <EncryptedKey xmlns="http://www.w3.org/2001/04/xmlenc#">
            Element encryptedKey = document.createElementNS(
                    "http://www.w3.org/2000/09/xmldsig#", "EncryptedKey");
            keyInfo.appendChild(encryptedKey);

            // Create: <KeyInfo>
            Element key_keyInfo = document.createElementNS(
                    "http://www.w3.org/2000/09/xmldsig#", "KeyInfo");
            keyInfo.appendChild(key_keyInfo);

            // Create: <KeyName/>
            Element keyName = document.createElement("KeyName");
            key_keyInfo.appendChild(keyName);

            // Create: <CipherData/>
            Element cipherData = document.createElement("CipherData");
            encryptedKey.appendChild(cipherData);

            // Create: <CipherValue/>
            Element cipherValue = document.createElement("CipherValue");
            cipherValue.appendChild(document.createTextNode(encryptedKeyData));
            encryptedKey.appendChild(cipherValue);

            // Create: <CipherData/>
            Element _cipherData = document.createElement("CipherData");
            documentElement.appendChild(_cipherData);

            // Create: <CipherValue/>
            Element _cipherValue = document.createElement("CipherValue");
            _cipherValue.appendChild(document.createTextNode(encryptedDataData));
            _cipherData.appendChild(_cipherValue);

            Transformer transformer = TransformerFactory.
                    newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(xmlFile);
            transformer.transform(source, result);

            logger.info("Wrote encrypted information to file");
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }
}
