package chestnut.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * This class uses the following XML as a template to produce signed
 * information:
 *
 * <Envelope xmlns="urn:envelope">
 * <dataToBeSigned>Data to be signed</dataToBeSigned>
 * <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
 * <SignedInfo>
 * <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>
 * <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/>
 * <Reference URI="">
 * <Transforms>
 * <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
 * </Transforms>
 * <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
 * <DigestValue/>
 * </Reference>
 * </SignedInfo>
 * <SignatureValue/>
 * <KeyInfo>
 * <KeyName/>
 * </KeyInfo>
 * </Signature>
 * </Envelope>
 */

public class SignatureController {

    private static final Logger logger =
            LoggerFactory.getLogger(SignatureController.class);

    private KeyController keyController;

    public SignatureController(KeyController keyController) {
        this.keyController = keyController;
    }

    /**
     * read an XML-document with <Envelope>. Locate the data
     * and signature and verify the signature.
     *
     * @param filename      name of the XML-file
     * @param publicKeyKey  Identifier showing the name of the private key to use
     * @param signatureType type  of signature e.g. SHA256withRSA
     * @return true if signature matches, false otherwise or if something
     * went wrong
     */
    public boolean readXMLFileAndVerifySignature(String filename,
                                                 String publicKeyKey,
                                                 String signatureType) {

        try {
            if (Files.exists(Paths.get(filename))) {
                InputStream xmlFile = new
                        FileInputStream(Paths.get(filename).toFile());
                return readXMLDataFromFileAndVerifySignature(xmlFile,
                        publicKeyKey, signatureType);
            } else {
                // you'd want an alert box here
                logger.error("File [" + filename + "] does not exist!");
            }
        } catch (IOException e) {
            logger.error(e.toString());
        }

        return false;
    }

    /**
     * Take a string of data and sign it. Then call a fucntion to write the
     * signed data and the data out to an XML document.
     *
     * @param data               The data to sign
     * @param filename           The name of the file to put data and
     *                           signature in
     * @param privateKeyKey      privateKey identifier used to retrieve private key
     * @param digestAlgorithm    The algorithm to use when creating a checksum
     * @param signatureAlgorithm The algorithm to use when signing
     * @throws NoSuchAlgorithmException algorithm is not supported
     * @throws InvalidKeyException      Problem with the key
     * @throws SignatureException       Something went wrong when signing
     */
    public String signDataAndWriteToXMLFile(String data, String filename,
                                            String privateKeyKey,
                                            String digestAlgorithm,
                                            String signatureAlgorithm)
            throws NoSuchAlgorithmException, IOException,
            InvalidKeyException, SignatureException {
        FileOutputStream xmlFile = new FileOutputStream(filename);

        //if (null == privateKey) {
        String signature = createSignature(data, privateKeyKey,
                signatureAlgorithm);
        String digest = createDigest(data, digestAlgorithm);
        writeXMLDataToFile(data, signature, digest, xmlFile);
        // }
        // else {
        //     throw new InvalidKeyException();
        // }
        return signature;
    }

    /**
     * Take data and signature and write it to the XML-file
     * <p>
     * Really we should be using XMLSignatureFactory
     * https://docs.oracle.com/javase/9/security/java-xml-digital-signature-api-overview-and-tutorial.htm
     * <p>
     * Leaving it like this for the moment, but all the namespaces etc should
     * be set automatically and not hardcoded
     *
     * @param data               The data to be written
     * @param signatureValueData The signature covering data
     * @param digestValueData    checksum covering the data
     * @param xmlFile            The file to write to
     */
    private void writeXMLDataToFile(String data, String signatureValueData,
                                    String digestValueData,
                                    OutputStream xmlFile) {

        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;

        try {

            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            // Create:  <Envelope xmlns="urn:envelope">
            Element documentElement = document.createElementNS("urn:envelope",
                    "Envelope");
            document.appendChild(documentElement);

            // Add the data, base64 encode it though
            Element dataElement = document.createElement("chestnut_data");
            dataElement.appendChild(document.createTextNode(data));
            documentElement.appendChild(dataElement);

            // Create:  <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
            Element signature = document.createElementNS(
                    "http://www.w3.org/2000/09/xmldsig#", "Signature");

            documentElement.appendChild(signature);

            // Create:  <SignedInfo>
            Element signedInfo = document.createElement("SignedInfo");
            signature.appendChild(signedInfo);

            // Create:  <CanonicalizationMethod Algorithm=
            // "http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>
            Element canonicalizationMethod = document.createElement
                    ("CanonicalizationMethod");
            canonicalizationMethod.setAttribute("Algorithm", "http://www.w3.org/TR/2001/REC-xml-c14n-20010315");
            signedInfo.appendChild(canonicalizationMethod);

            // Create:  <SignatureMethod Algorithm=
            // "http://www.w3.org/2000/09/xmldsig#rsa-sha1"/>
            Element signatureMethod = document.createElement("SignatureMethod");
            signatureMethod.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#rsa-sha1");
            signedInfo.appendChild(signatureMethod);

            // Create: <Reference URI="">
            Element reference = document.createElement("Reference");
            reference.setAttribute("URI", "");
            signedInfo.appendChild(reference);

            // Create: <Transforms>
            Element transforms = document.createElement("Transforms");
            reference.appendChild(transforms);

            // Create: <Transform Algorithm=
            // "http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
            Element transform = document.createElement("Transform");
            transform.setAttribute("Algorithm", "http://www" +
                    ".w3.org/2000/09/xmldsig#rsa-sha1");
            transforms.appendChild(transform);

            // Create: <DigestMethod Algorithm=
            // "http://www.w3.org/2000/09/xmldsig#sha256"/>
            Element digestMethod = document.createElement("DigestMethod");
            digestMethod.setAttribute("Algorithm",
                    "http://www.w3.org/2000/09/xmldsig#sha256");
            reference.appendChild(digestMethod);

            // Create:  <DigestValue>
            Element digestValue = document.createElement("DigestValue");
            digestValue.appendChild(document.createTextNode(digestValueData));
            reference.appendChild(digestValue);

            // Create: <SignatureValue>
            Element signatureValue = document.createElement("SignatureValue");

            signatureValue.appendChild(document.createTextNode
                    (signatureValueData));
            signature.appendChild(signatureValue);

            // Create: <KeyInfo>
            Element keyInfo = document.createElement("KeyInfo");
            signature.appendChild(keyInfo);

            // Create: <KeyName/>
            Element keyName = document.createElement("KeyName");
            keyInfo.appendChild(keyName);

            Transformer transformer = TransformerFactory.
                    newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(xmlFile);
            transformer.transform(source, result);
            logger.info("Wrote signature information to file");
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sign data with the given private key.
     *
     * @param dataAsString  The data to sign
     * @param privateKeyKey Identifier of the private key to sign with
     * @param algorithm     the algorithm to use when signing e.g. SHA256withRSA
     * @return The signed data base64 encoded
     * @throws NoSuchAlgorithmException Algorithm not supported
     * @throws InvalidKeyException      Problem with the key
     * @throws SignatureException       Problem with the signature
     */
    public String createSignature(String dataAsString, String privateKeyKey,
                                  String algorithm)
            throws NoSuchAlgorithmException,
            InvalidKeyException, SignatureException {

        PrivateKey privateKey = keyController.getPrivateKey(privateKeyKey);
        byte[] data = dataAsString.getBytes(UTF_8);
        Signature signature = Signature.getInstance(algorithm);
        signature.initSign(privateKey);
        signature.update(data);
        byte[] signatureBytes = signature.sign();
        return Base64.getEncoder().
                encodeToString(signatureBytes);
    }

    /**
     * Create a checksum for a given amount of data
     *
     * @param dataAsString data to create checksum for
     * @param algorithm    type when calculating checksum e.g.
     * @return The checksum value
     * @throws NoSuchAlgorithmException Algorithm not supported
     */
    private String createDigest(String dataAsString, String algorithm)
            throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance(algorithm);
        byte[] hash = digest.digest(dataAsString.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(hash);
    }

    /**
     * Traverse the XML-document and retrieve the signature value
     *
     * @param document The document loaded in memory
     * @return the signature of the data that was signed
     */
    private String getSignatureValue(Document document) {

        String signatureValue = null;

        NodeList nodeList = document.getElementsByTagName("SignatureValue");
        if (nodeList.getLength() < 1) {
            logger.info("Attempt to open file without a signature value");
            return null;
        } else if (nodeList.getLength() > 1) {
            logger.info("Attempt to open file without multiple signature " +
                    "values");
            return null;
        }

        Node node = nodeList.item(0);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            signatureValue = element.getTextContent();
            logger.info("Found signature value: [" + signatureValue + "]");
        }
        return signatureValue;
    }

    /**
     * Traverse the XML-document and retrieve the checksum value
     *
     * @param document The document loaded in memory
     * @return the checksum of the data
     */
    private String getChecksum(Document document) {

        String digestValue = null;

        NodeList nodeList = document.getElementsByTagName("DigestValue");
        if (nodeList.getLength() < 1) {
            logger.info("Attempt to open file without a DigestValue ");
            return null;
        } else if (nodeList.getLength() > 1) {
            logger.info("Attempt to open file without multiple DigestValue" +
                    "values");
            return null;
        }

        Node node = nodeList.item(0);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            digestValue = element.getTextContent();
            logger.info("Found DigestValue : [" + digestValue + "]");
        }
        return digestValue;
    }

    /**
     * Traverse the XML-document and retrieve the data payload that was signed
     *
     * @param document The document loaded in memory
     * @return the data that was signed
     */
    private String getData(Document document) {

        String data = null;

        NodeList nodeList = document.getElementsByTagName("chestnut_data");

        if (nodeList.getLength() < 1) {
            logger.info("Attempt to open file without any dataToBeSigned");
            return null;
        } else if (nodeList.getLength() > 1) {
            logger.info("Attempt to open file without multiple dataToBeSigned");
            return null;
        }

        Node node = nodeList.item(0);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            data = element.getTextContent();
            logger.info("Found data value: [" + data + "]");
        }
        return data;
    }

    /**
     * read data from an XML-file and attempt to verify that the encapsulated
     * data and signature are correct.
     *
     * @param xmlFile       The file to read from
     * @param publicKeyKey  Identifier of the public key to verify with
     * @param signatureType type of signature e.g. SHA256withRSA
     * @return true if data is signed with a private key corresponding to the
     * given public key
     */
    private boolean readXMLDataFromFileAndVerifySignature(
            InputStream xmlFile, String publicKeyKey, String signatureType) {

        try {
            StringBuilder data = new StringBuilder();
            StringBuilder signature = new StringBuilder();
            StringBuilder checksum = new StringBuilder();

            readXMLDataFromFile(xmlFile, data, signature, checksum);
            return verifySignature(data.toString().getBytes(UTF_8),
                    Base64.getDecoder().decode(signature.toString()),
                    publicKeyKey, signatureType);
        } catch (NoSuchAlgorithmException | InvalidKeyException |
                SignatureException e) {
            logger.error(e.toString());
        }
        return false;
    }

    public void readXMLDataFromFile(InputStream xmlFile,
                                    StringBuilder data,
                                    StringBuilder signature,
                                    StringBuilder checksum) {

        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;

        try {

            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);

            data.append(getData(document));
            signature.append(getSignatureValue(document));
            checksum.append(getSignatureValue(document));

        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error(e.toString());
        }
    }


    /**
     * Check that signed data is verifiable with the given public key.
     *
     * @param data           data that signature covers
     * @param signatureBytes The signature covering the data
     * @param publicKeyKey   the public key yo verify with
     * @param signatureType  type of signature e.g. SHA256withRSA
     * @return true if valid, false otherwise
     * @throws NoSuchAlgorithmException algorithm not supported
     * @throws InvalidKeyException      problem with key
     * @throws SignatureException       problem with the signature
     */
    private boolean verifySignature(byte[] data, byte[] signatureBytes,
                                    String publicKeyKey, String signatureType)
            throws NoSuchAlgorithmException, InvalidKeyException,
            SignatureException {
        PublicKey publicKey = keyController.getPublicKey(publicKeyKey);
        Signature signature = Signature.getInstance(signatureType);
        signature.initVerify(publicKey);
        signature.update(data);

        return signature.verify(signatureBytes);
    }

    /**
     * Helper method to allow the use of strings
     *
     * @param data           data
     * @param signatureBytes base64encoded signature
     * @param publicKeyKey   publid key identifier
     * @param signatureType  algorithm used
     * @return true if verified, false otherwise
     */

    public boolean verifySignatureEncodedBase64(
            String data, String signatureBytes,
            String publicKeyKey, String signatureType)
            throws NoSuchAlgorithmException, InvalidKeyException,
            SignatureException {
        return verifySignature(
                data.getBytes(UTF_8),
                Base64.getDecoder().decode(signatureBytes),
                publicKeyKey, signatureType);
    }
}
